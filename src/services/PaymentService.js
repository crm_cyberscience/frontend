import { payment } from 'utils/api_urls'
import Request from 'utils/request'

const PaymentService = {}

PaymentService.PaymentCreate = function (data) {
	return Request.postRequest(`${payment}/add`,data)
}

PaymentService.PaymentList = function ({pageNumber,pageSize,studentID=''}) {
	return Request.getRequest(`${payment}?pageNumber=${pageNumber}&pageSize=${pageSize}&studentID=${studentID}`)
}

PaymentService.CalculateMonth = function () {
	return Request.getRequest(`${payment}/calculateMonth`)
}

PaymentService.CalculateMonthSystem = function () {
	return Request.getRequest(`${payment}/calculateMonthSystem`)
}

PaymentService.Delete = function (data) {
	return Request.deleteRequest(`${payment}/delete?id=${data}`)
}

PaymentService.CalculateGroup = function (data) {
	return Request.postRequest(`${payment}/calculateGroup`,data)
}

export default PaymentService