import {
  AppstoreOutlined,
  FundOutlined,
  RollbackOutlined,
  UserOutlined,
  AuditOutlined,
  UsergroupAddOutlined,
  OrderedListOutlined,
  UserAddOutlined,
  FileDoneOutlined,
  MoneyCollectFilled,
  BookOutlined,
  DollarOutlined,
  WalletOutlined,
  BarChartOutlined,
  ExportOutlined,
  CalendarOutlined,
  HistoryOutlined,
  DeleteOutlined
} from '@ant-design/icons';

import { APP_PREFIX_PATH, AUTH_PREFIX_PATH } from 'configs/AppConfig';



const appsNavTree = [{
  key: 'apps',
  path: `${APP_PREFIX_PATH}/apps`,
  title: 'Dashboard',
  icon: AppstoreOutlined,
  breadcrumb: false,
  isGroupTitle: true,
  submenu: [
    // {
    //   key: 'apps-leads',
    //   path: `${APP_PREFIX_PATH}/leads`,
    //   title: 'Leads',
    //   icon: RollbackOutlined,
    //   breadcrumb: false,
    //   submenu: []
    // },
    {
      key: 'apps-teachers',
      path: `${APP_PREFIX_PATH}/apps/teachers`,
      title: 'Teachers',
      icon: AuditOutlined,
      breadcrumb: true,
      submenu: [
        {
          key: 'apps-teachers-teachersList',
          path: `${APP_PREFIX_PATH}/apps/teachers/teacher-list`,
          title: 'Teachers',
          icon: OrderedListOutlined,
          breadcrumb: true,
          submenu: []
        },
        {
          key: 'apps-teachers-addTeacher',
          path: `${APP_PREFIX_PATH}/apps/teachers/add-teacher`,
          title: 'Add Teacher',
          icon: UserAddOutlined,
          breadcrumb: false,
          submenu: []
        },
        // {
        //   key: 'apps-teachers-editTeacher',
        //   path: `${APP_PREFIX_PATH}/apps/teachers/edit-teacher/12`,
        //   title: 'Edit Teacher',
        //   icon: FileDoneOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // }
      ]
    },
    {
      key: 'apps-students',
      path: `${APP_PREFIX_PATH}/apps/students`,
      title: 'Students',
      icon: UserOutlined,
      breadcrumb: true,
      submenu: [
        {
          key: 'apps-students-studentsList',
          path: `${APP_PREFIX_PATH}/apps/students/student-list`,
          title: 'Students',
          icon: OrderedListOutlined,
          breadcrumb: true,
          submenu: []
        },
        {
          key: 'apps-students-addStudent',
          path: `${APP_PREFIX_PATH}/apps/students/add-student`,
          title: 'Add Student',
          icon: UserAddOutlined,
          breadcrumb: false,
          submenu: []
        },
        // {
        //   key: 'apps-students-editStudent',
        //   path: `${APP_PREFIX_PATH}/apps/students/edit-student/12`,
        //   title: 'Edit Student',
        //   icon: FileDoneOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // }
      ]
    },
    {
      key: 'apps-groups',
      path: `${APP_PREFIX_PATH}/apps/groups`,
      title: 'Groups',
      icon: UsergroupAddOutlined,
      breadcrumb: true,
      submenu: [
        {
          key: 'apps-groups-groupsList',
          path: `${APP_PREFIX_PATH}/apps/groups/group-list`,
          title: 'Groups',
          icon: OrderedListOutlined,
          breadcrumb: true,
          submenu: []
        },
        {
          key: 'apps-groups-addGroup',
          path: `${APP_PREFIX_PATH}/apps/groups/add-group`,
          title: 'Add Group',
          icon: UserAddOutlined,
          breadcrumb: false,
          submenu: []
        },
        // {
        //   key: 'apps-groups-editGroup',
        //   path: `${APP_PREFIX_PATH}/apps/groups/edit-group/12`,
        //   title: 'Edit Group',
        //   icon: FileDoneOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // }
      ]
    },
    {
      key: 'apps-course',
      path: `${APP_PREFIX_PATH}/apps/course`,
      title: 'Course',
      icon: BookOutlined,
      breadcrumb: true,
      submenu: [
        {
          key: 'apps-course-courseList',
          path: `${APP_PREFIX_PATH}/apps/course/course-list`,
          title: 'Courses',
          icon: OrderedListOutlined,
          breadcrumb: true,
          submenu: []
        },
        {
          key: 'apps-course-addCourse',
          path: `${APP_PREFIX_PATH}/apps/course/add-course`,
          title: 'Add Course',
          icon: UserAddOutlined,
          breadcrumb: false,
          submenu: []
        },
        // {
        //   key: 'apps-course-editCourse',
        //   path: `${APP_PREFIX_PATH}/apps/course/edit-course/12`,
        //   title: 'Edit Course',
        //   icon: FileDoneOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // }
      ]
    },
    {
      key: 'dashboards-all-payments',
      path: `${APP_PREFIX_PATH}/dashboards/all-payments`,
      title: 'Finance',
      icon: DollarOutlined,
      breadcrumb: false,
      submenu: [
        {
          key: 'dashboards-all-payments',
          path: `${APP_PREFIX_PATH}/dashboards/all-payments`,
          title: 'All Payments',
          icon: FundOutlined,
          breadcrumb: false,
          submenu: []
        },
        // {
        //   key: 'dashboards-withdraw',
        //   path: `${APP_PREFIX_PATH}/dashboards/withdraw`,
        //   title: 'Withdraw',
        //   icon: ExportOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // },
        // {
        //   key: 'dashboards-total-expenses',
        //   path: `${APP_PREFIX_PATH}/dashboards/total-expenses`,
        //   title: 'Total Expenses',
        //   icon: BarChartOutlined
        //   ,
        //   breadcrumb: false,
        //   submenu: []
        // },
        // {
        //   key: 'dashboards-salaries',
        //   path: `${APP_PREFIX_PATH}/dashboards/salaries`,
        //   title: 'Salaries',
        //   icon: WalletOutlined,
        //   breadcrumb: false,
        //   submenu: []
        // },
      ]
    },
    {
      key: 'history',
      path: `${APP_PREFIX_PATH}/apps/students`,
      title: 'History',
      icon: HistoryOutlined,
      breadcrumb: false,
      submenu: [
        {
          key: 'history-page',
          path: `${APP_PREFIX_PATH}/apps/students/history`,
          title: 'History Student',
          icon: DeleteOutlined,
          breadcrumb: false,
          submenu: []
        }
      ]
    },
    {
      key: 'components-calendar',
      path: `${APP_PREFIX_PATH}/apps/calendar`,
      title: 'Calendar',
      icon: CalendarOutlined,
      breadcrumb: true,
      submenu: []
    },
  ]
}]


const navigationConfig = [
  ...appsNavTree
]

export default navigationConfig;
