import React, { Component } from 'react';
import { Form, Input, Row, Col, Select, TimePicker, DatePicker } from 'antd';
import { ROW_GUTTER } from 'constants/ThemeConstant';


const { Option } = Select;
const { RangePicker: RangePickerTime } = TimePicker;
const { RangePicker: RangePickerDate } = DatePicker;
const { TextArea } = Input;

const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const GeneralField = (props) => {

	return (
		<Row className="mt-4">
			<Col xs={24} sm={24} md={24} lg={16}>
				<Row gutter={ROW_GUTTER}>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Course Name"
							name="name"
							rules={[
								{
									required: true,
									message: 'Please input course name!',
								},
							]}
						>
							<Input />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={12}>
						<Form.Item
							label="Comment"
							name="comment"
						>
							<TextArea
								// value={value}
								// onChange={this.onChange}
								placeholder="Comment ..."
								autoSize={{ minRows: 3, maxRows: 5 }}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default GeneralField;

