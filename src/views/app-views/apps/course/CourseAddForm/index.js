import React, { useState, useEffect } from 'react'
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import { Tabs, Form, Button, message } from 'antd';
import Flex from 'components/shared-components/Flex'
import GeneralField from './GeneralField'
import { useNavigate } from "react-router-dom"
import ProductListData from "assets/data/product-list.data.json"
import { useDispatch } from 'react-redux';
import CourseService from 'services/CourseService';
import { changeStatus } from 'store/slices/courseSlice';
import AuthService from 'services/AuthService';

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

const ADD = 'ADD'
const EDIT = 'EDIT'

const CourseAddForm = props => {

	const { mode = ADD, param } = props
	const navigate = useNavigate();


	const [form] = Form.useForm();
	const [uploadedImg, setImage] = useState('')
	const [uploadLoading, setUploadLoading] = useState(false)
	const [submitLoading, setSubmitLoading] = useState(false)
	const dispatch = useDispatch()

	useEffect(() => {
    	if(mode === EDIT) {
			console.log('is edit')
			console.log('props', props)
			const { id } = param
			const produtId = parseInt(id)
			const productData = ProductListData.filter( product => product.id === produtId)
			const product = productData[0]
			form.setFieldsValue({
				comparePrice: 0.00,
				cost: 0.00,
				taxRate: 6,
				description: 'There are many variations of passages of Lorem Ipsum available.',
				category: product.category,
				name: product.name,
				price: product.price
			});
			setImage(product.image)
		}
  	}, [form, mode, param, props]);

	const handleUploadChange = info => {
		if (info.file.status === 'uploading') {
			setUploadLoading(true)
			return;
		}
		if (info.file.status === 'done') {
			getBase64(info.file.originFileObj, imageUrl =>{
				setImage(imageUrl)
				setUploadLoading(true)
			});
		}
	};

	const navigateTeacher = () => {
		navigate(`/app/apps/course/course-list`)
	}

	const onFinish = () => {
		setSubmitLoading(true)
		form.validateFields().then(values => {
			setTimeout(async () => {
				// let data=handleData(values.startOfLessonTime)
				// values.start_time=data[0]
				// values.end_time=data[1]
				// data=handleDataCourse(values.startOfCourseDate)
				// values.start_date=data[0]
				// values.end_date=data[1]
				const user=await AuthService.me()
				console.log(values);
				values.created_by=user?.data?.user[0]?.first_name
				setSubmitLoading(false)
				CourseService.CourseCreate(values).then((data) => {
					if (mode === ADD) {
						message.success(`Created ${values.name} to course list`);
					}
					if (mode === EDIT) {
						message.success(`Course saved`);
					}
					dispatch(changeStatus())
					navigateTeacher()
				}).catch((error) => {
					console.log(`error`)
					message.error(`error`);
					console.log(error)
				});
				setSubmitLoading(false)
			}, 1500);
		}).catch(info => {
			setSubmitLoading(false)
			console.log('info', info)
			message.error('Please enter all required field ');
		});
	};

	return (
		<>
			<Form
				layout="vertical"
				form={form}
				name="advanced_search"
				className="ant-advanced-search-form"
				initialValues={{
					heightUnit: 'cm',
					widthUnit: 'cm',
					weightUnit: 'kg'
				}}
			>
				<PageHeaderAlt className="border-bottom" overlap>
					<div className="container">
						<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
							<h2 className="mb-3">{mode === 'ADD'? 'Add New Course' : `Edit Course`} </h2>
							<div className="mb-3">
								<Button className="mr-2">Discard</Button>
								<Button type="primary" onClick={() => onFinish()} htmlType="submit" loading={submitLoading} >
									{mode === 'ADD'? 'Add' : `Save`}
								</Button>
							</div>
						</Flex>
					</div>
				</PageHeaderAlt>
				<div className="container">
					<Tabs 
						defaultActiveKey="1" 
						style={{marginTop: 30}}
						items={[
							{
								label: 'General',
								key: '1',
								children: <GeneralField 
									uploadedImg={uploadedImg} 
									uploadLoading={uploadLoading} 
									handleUploadChange={handleUploadChange}
								/>,
							}
							
						]}
					/>
				</div>
			</Form>
		</>
	)
}

export default CourseAddForm
