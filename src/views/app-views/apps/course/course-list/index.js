import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Input, Button, Badge, Menu, message } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { EyeOutlined, DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined } from '@ant-design/icons';
import AvatarStatus from 'components/shared-components/AvatarStatus';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changeStatus, getCourseError, getCourseList, getCourseStatus, getCourses, getPageNumber, getPageSize } from 'store/slices/courseSlice';
import { getDateString } from 'utils/aditionalFunctions';
import CourseService from 'services/CourseService';

const { Option } = Select
const categories = ['Cloths', 'Bags', 'Shoes', 'Watches', 'Devices']

const CourseList = () => {
	const navigate = useNavigate();
	const [list, setList] = useState(ProductListData)
	const [selectedRows, setSelectedRows] = useState([])
	const [search, setSearch] = useState('')
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	const dispatch = useDispatch()
	const courseList = useSelector(getCourseList)
	const courseStatus = useSelector(getCourseStatus)
	const courseError = useSelector(getCourseError)
	const pageNumber = useSelector(getPageNumber)
	const pageSize = useSelector(getPageSize)

	useEffect(() => {
		if (courseStatus === 'idle') {
			dispatch(getCourses({ pageNumber: pageNumber, pageSize: pageSize, query: search }))
		}
	}, [courseStatus, dispatch])


	const dropdownMenu = row => (
		<Menu>
			<Menu.Item onClick={() => viewDetails(row)}>
				<Flex alignItems="center">
					< EditOutlined />
					<span className="ml-2">Edit</span>
				</Flex>
			</Menu.Item>
			<Menu.Item onClick={() => deleteRow(row)}>
				<Flex alignItems="center">
					<DeleteOutlined />
					<span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span>
				</Flex>
			</Menu.Item>
		</Menu>
	);

	const addCourse = () => {
		navigate(`/app/apps/course/add-course`)
	}

	const viewDetails = row => {
		navigate(`/app/apps/groups/edit-group/${row.id}`)
	}

	const deleteRow = (row) => {
		if (selectedRows.length > 1) {
			selectedRows.forEach((elm) => {
				CourseService.CourseDelete(elm._id)
					.then((data) => {
						message.success(`Deleted ${elm.name} from Course`);
						dispatch(changeStatus());
						setSelectedRows([])
					})
					.catch((error) => {
						console.log(`error`);
						message.error(`error`);
						console.log(error);
					});
			});
		} else {
			CourseService.CourseDelete(row._id)
				.then((data) => {
					message.success(`Deleted ${row.name} from Course`);
					dispatch(changeStatus());
					setSelectedRows([])
				})
				.catch((error) => {
					console.log(`error`);
					message.error(`error`);
					console.log(error);
				});
		}
	};

	const tableColumns = [
		{
			title: 'Course Name',
			dataIndex: 'name',

			sorter: (a, b) => utils.antdTableSorter(a, b, 'name')
		},
		{
			title: 'Comment',
			dataIndex: 'comment',
		},
		{
			title: 'Created By',
			dataIndex: 'created_by',
		},
		{
			title: 'Created At',
			dataIndex: 'created_at',
		},
		{
			title: 'Updated At',
			dataIndex: 'updated_at',
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			render: (_, elm) => (
				<div className="text-right">
					<EllipsisDropdown menu={dropdownMenu(elm)} />
				</div>
			)
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value
		const searchArray = e.currentTarget.value ? list : ProductListData
		const data = utils.wildCardSearch(searchArray, value)
		setList(data)
		setSelectedRowKeys([])
	}

	const handleShowCategory = value => {
		if (value !== 'All') {
			const key = 'category'
			const data = utils.filterArray(ProductListData, key, value)
			setList(data)
		} else {
			setList(ProductListData)
		}
	}

	return (
		<Card>
			<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
				<Flex className="mb-1" mobileFlex={false}>
					<div className="mr-md-3 mb-3">
						<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
					</div>
					{/* <div className="mb-3">
						<Select
							defaultValue="All"
							className="w-100"
							style={{ minWidth: 180 }}
							onChange={handleShowCategory}
							placeholder="Category"
						>
							<Option value="All">All</Option>
							{
								categories.map(elm => (
									<Option key={elm} value={elm}>{elm}</Option>
								))
							}
						</Select>
					</div> */}
				</Flex>
				<div>
					<Button onClick={addCourse} type="primary" icon={<PlusCircleOutlined />} block>Add Course</Button>
				</div>
			</Flex>
			<div className="table-responsive">
				<Table
					columns={tableColumns}
					dataSource={courseList.map(elem => {
						return {
							...elem,
							end_date: getDateString(elem.end_date),
							start_date: getDateString(elem.start_date),
							created_at: getDateString(elem.created_at),
							updated_at: getDateString(elem.updated_at),
							// schedule: elem.schedule.join(",")
						}
					})}
					rowKey='id'
					rowSelection={{
						selectedRowKeys: selectedRowKeys,
						type: 'checkbox',
						preserveSelectedRowKeys: false,
						...rowSelection,
					}}
				/>
			</div>
		</Card>
	)
}

export default CourseList
