import React from 'react'
import CourseEditForm from '../CourseEditForm';
import { useParams } from 'react-router-dom';

const EditCourse = () => {
	const params = useParams();

	return (
		<CourseEditForm mode="EDIT" param={params}/>
	)
}

export default EditCourse
