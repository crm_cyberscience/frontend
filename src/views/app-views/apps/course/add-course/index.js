import React from 'react';
import CourseAddForm from '../CourseAddForm';

const AddCourse = () => {
	return (
		<CourseAddForm mode="ADD"/>
	)
}

export default AddCourse
