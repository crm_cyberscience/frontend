import React, { Component } from 'react';
import { Form, Button, Input, DatePicker, Row, Col, message, Select, TimePicker } from 'antd';
import { ROW_GUTTER } from 'constants/ThemeConstant';


const { Option } = Select;
const { RangePicker: RangePickerTime } = TimePicker;
const { TextArea } = Input;

const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const GeneralField = (props) => {

	const onChange = (e) => {
		console.log(e.target.values)
	};

	return (
		<Row className="mt-4">
			<Col xs={24} sm={24} md={24} lg={16}>
				<Row gutter={ROW_GUTTER}>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Course Name"
							name="name"
							rules={[
								{
									required: true,
									message: 'Please input course name!',
								},
							]}
						>
							<Input />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="price"
							label="Price"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Input />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="day"
							label="Select Days"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Select
								mode="multiple"
								style={{ width: "100%" }}
								placeholder="Please select"
							// defaultValue={selectedDays}
							// onChange={handleChange}
							>
								{weekdays.map(day => (
									<Option key={day} value={day}>{day}</Option>
								))}
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="length"
							label="Length"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Input />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Lesson start/end time"
							name="startOfLessonTime"
						>
							<RangePickerTime />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Created By"
							name="created_by"
						>
							<Select />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={12}>
						<Form.Item
							label="Comment"
							name="comment"
						>
							<TextArea
								// value={value}
								// onChange={this.onChange}
								placeholder="Comment ..."
								autoSize={{ minRows: 3, maxRows: 5 }}
							/>
						</Form.Item>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default GeneralField;

