import React from 'react';
import GroupAddForm from '../GroupAddForm';

const AddGroup = () => {
	return (
		<GroupAddForm mode="ADD"/>
	)
}

export default AddGroup
