import React from 'react';
import {Row, Col, Form, Select } from 'antd';
import { ROW_GUTTER } from 'constants/ThemeConstant';

const { Option } = Select;


const GeneralField = (props) => {

	const onChange = (e) => {
		
	};

	return (
		<Row className="mt-4">
			<Col xs={24} sm={24} md={24} lg={16}>
				<Row gutter={ROW_GUTTER}>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="student"
							label="Select Student"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Select
								placeholder="Select a option and change input text above"
								onChange={onChange}
								allowClear
							>
								<Option value="1">Anvar</Option>
								<Option value="2">Alsu</Option>
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="course"
							label="Select Course"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Select
								placeholder="Select a option and change input text above"
								onChange={onChange}
								allowClear
							>
								<Option value="1">Fizika</Option>
								<Option value="2">python</Option>
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="teacher"
							label="Select Teacher"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Select
								placeholder="Select a option and change input text above"
								onChange={onChange}
								allowClear
							>
								<Option value="1">Rodion</Option>
								<Option value="2">Alsu</Option>
							</Select>
						</Form.Item>
					</Col>
				</Row>
			</Col>
		</Row>
	);
};

export default GeneralField;
