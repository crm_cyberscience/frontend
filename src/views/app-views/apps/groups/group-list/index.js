import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Input, Button, Badge, Menu, message, Tooltip, Switch } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { EyeOutlined, DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined, InfoCircleOutlined } from '@ant-design/icons';
import AvatarStatus from 'components/shared-components/AvatarStatus';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changeStatus, getGroupError, getGroupList, getGroupPageNumber, getGroupPageSize, getGroups, getGroupStatus } from 'store/slices/groupSlice';
import GroupService from 'services/GroupService';
import { getDateDayString, getDateString } from 'utils/aditionalFunctions';

const { Option } = Select
const categories = ['Cloths', 'Bags', 'Shoes', 'Watches', 'Devices']

const GroupList = () => {
	const navigate = useNavigate();
	const [list, setList] = useState(ProductListData)
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	const dispatch = useDispatch()
	const groupList = useSelector(getGroupList)
	const groupStatus = useSelector(getGroupStatus)
	const groupError = useSelector(getGroupError)
	const pageNumber = useSelector(getGroupPageNumber)
	const pageSize = useSelector(getGroupPageSize)

	useEffect(() => {
		if (groupStatus === 'idle') {
			dispatch(getGroups({ pageNumber: pageNumber, pageSize: pageSize }))
		}
	}, [groupStatus, dispatch])

	useEffect(() => {
		setList(groupList);
	}, [groupList]);


	const dropdownMenu = row => (
		<Menu>
			<Menu.Item onClick={() => viewDetails(row)}>
				<Flex alignItems="center">
					< EditOutlined />
					<span className="ml-2">Edit</span>
				</Flex>
			</Menu.Item>
			<Menu.Item onClick={() => deleteRow(row)}>
				<Flex alignItems="center">
					<DeleteOutlined />
					<span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span>
				</Flex>
			</Menu.Item>
		</Menu>
	);

	const addGroup = () => {
		navigate(`/app/apps/groups/add-group`)
	}

	const viewDetails = row => {
		navigate(`/app/apps/groups/edit-group/${row._id}`)
	}

	const viewInfo = (row) => {
		navigate(`/app/apps/groups/group-student/${row._id}`);
	}

	const deleteRow = (row) => {
		if (selectedRows.length > 1) {
			selectedRows.forEach((elm) => {
				GroupService.GroupDelete(elm._id)
					.then((data) => {
						message.success(`Deleted ${elm.name} from teacher`);
						dispatch(changeStatus());
					})
					.catch((error) => {
						console.log(`error`);
						message.error(`error`);
						console.log(error);
					});
			});
		} else {
			GroupService.GroupDelete(row._id)
				.then((data) => {
					message.success(`Deleted ${row.name} from teacher`);
					dispatch(changeStatus());
				})
				.catch((error) => {
					console.log(`error`);
					message.error(`error`);
					console.log(error);
				});
		}
	};

	const handleStatusChange = async (checked, record) => {
		try {
			const updatedStatus = checked ? 'active' : 'inactive';
			// await dispatch(changeStatus({ id: record._id, status: updatedStatus }));
			await GroupService.ChangeGroupSts({groupId: record._id,status:updatedStatus })
			message.success(`Status changed to ${updatedStatus}`);
			// Optionally, refresh the list
			// const groups=await GroupService.GetByStudent({studentId:idStudent});
			// setList(groups?.data?.group)
			dispatch(changeStatus())
		} catch (error) {
			message.error('Failed to change status');
		}
	};

	const tableColumns = [
		{
			title: 'Name',
			dataIndex: 'name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'name')
		},
		{
			title: 'Teacher Name',
			dataIndex: 'teacher_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'price')
		},
		{
			title: 'Course name',
			dataIndex: 'course_name',
		},
		{
			title: 'Price',
			dataIndex: 'price',
		},
		{
			title: 'Schedule',
			dataIndex: 'schedule',
		},
		{
			title: 'Length',
			dataIndex: 'length',
		},
		{
			title: 'Type',
			dataIndex: 'type',
		},
		{
			title: 'Start Date',
			dataIndex: 'start_date',
		},
		{
			title: 'End Date',
			dataIndex: 'end_date',
		},
		// {
		// 	title: 'Created By',
		// 	dataIndex: 'created_by',
		// },
		// {
		// 	title: 'Created At',
		// 	dataIndex: 'created_at',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'created_at')
		// },
		// {
		// 	title: 'Updated At',
		// 	dataIndex: 'updated_at',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'updated_at')
		// },
		{
			title: 'Info',
			dataIndex: 'info',
			render: (_, elm) => (
				<Tooltip title="View Info">
					<InfoCircleOutlined
						style={{ color: 'green', cursor: 'pointer' }}
						onClick={() => viewInfo(elm)}
					/>
				</Tooltip>
			),
		},
		{
			title: 'Status',
			dataIndex: 'status',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'status'),
			render: (text, record) => (
				<Switch 
					// onClick={()=>{console.log(text);}}
					checked={text === 'active'}
					onChange={(checked) => handleStatusChange(checked, record)}
				/>
			)
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			render: (_, elm) => (
				<div className="text-right">
					<EllipsisDropdown menu={dropdownMenu(elm)} />
				</div>
			)
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value
		const searchArray = e.currentTarget.value ? list : ProductListData
		const data = utils.wildCardSearch(searchArray, value)
		setList(data)
		setSelectedRowKeys([])
	}

	const handleShowCategory = value => {
		if (value !== 'All') {
			const key = 'category'
			const data = utils.filterArray(ProductListData, key, value)
			setList(data)
		} else {
			setList(ProductListData)
		}
	}

	return (
		<Card>
			<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
				<Flex className="mb-1" mobileFlex={false}>
					<div className="mr-md-3 mb-3">
						<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
					</div>
					{/* <div className="mb-3">
						<Select
							defaultValue="All"
							className="w-100"
							style={{ minWidth: 180 }}
							onChange={handleShowCategory}
							placeholder="Category"
						>
							<Option value="All">All</Option>
							{
								categories.map(elm => (
									<Option key={elm} value={elm}>{elm}</Option>
								))
							}
						</Select>
					</div> */}
				</Flex>
				<div>
					<Button onClick={addGroup} type="primary" icon={<PlusCircleOutlined />} block>Add Group</Button>
				</div>
			</Flex>
			<div className="table-responsive">
				<Table
					columns={tableColumns}
					dataSource={list.map((elem) => {
						return {
							...elem,
							teacher_name: [elem?.teacherId?.first_name, elem?.teacherId?.last_name, elem?.teacherId?.middle_name].filter(Boolean).join(' '),
							course_name: elem?.courseId?.name,
							start_date: getDateDayString(elem.start_date),
							end_date: getDateDayString(elem.end_date),
						};
					})}
					rowKey='_id'
					rowSelection={{
						selectedRowKeys: selectedRowKeys,
						type: 'checkbox',
						preserveSelectedRowKeys: false,
						...rowSelection,
					}}
				/>
			</div>
		</Card>
	)
}

export default GroupList
