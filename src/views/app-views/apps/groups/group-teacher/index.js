import React, { useEffect, useState } from 'react'
import { Card, Table, Input, Button, Menu, Tooltip } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined, InfoCircleOutlined } from '@ant-design/icons';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate, useParams } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changeStatusStudent, getPageNumber, getPageSize, getStudents, getStudentsError, getStudentsList, getStudentsStatus } from 'store/slices/studentSlice';
import { getDateString } from 'utils/aditionalFunctions';
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import GroupService from 'services/GroupService';

const TeacherList = () => {
	const { id } = useParams()
	const navigate = useNavigate();
	const [list, setList] = useState([])
	const [search, setSearch] = useState('')
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	const dispatch = useDispatch()
	const studentList = useSelector(getStudentsList)
	const studentStatus = useSelector(getStudentsStatus)
	const studentError = useSelector(getStudentsError)
	const pageNumber = useSelector(getPageNumber)
	const pageSize = useSelector(getPageSize)

	useEffect(() => {
		const fetchData = async () => {
			const getStudents = await GroupService.GetStudents({ groupId: id })
			console.log(getStudents?.data?.students[0]?.students);
			setList(getStudents?.data?.students[0]?.students)
		}
		fetchData()
	}, [id])

	const dropdownMenu = row => (
		<Menu>
			<Menu.Item onClick={() => viewDetails(row)}>
				<Flex alignItems="center">
					< EditOutlined />
					<span className="ml-2">Edit</span>
				</Flex>
			</Menu.Item>
			<Menu.Item onClick={() => deleteRow(row)}>
				<Flex alignItems="center">
					<DeleteOutlined />
					<span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span>
				</Flex>
			</Menu.Item>
		</Menu>
	);

	const addTeacher = () => {
		navigate(`/app/apps/teachers/add-teacher`)
	}

	const viewDetails = row => {
		navigate(`/app/apps/teachers/edit-teacher/${row?.teacherId}`)
	}

	const viewInfo = (row) => {
		navigate(`/app/apps/teachers/info-teacher/${row?.teacherId}`);
	};

	const deleteRow = row => {
		const objKey = 'id'
		let data = list
		if (selectedRows.length > 1) {
			selectedRows.forEach(elm => {
				data = utils.deleteArrayRow(data, objKey, elm.id)
				setList(data)
				setSelectedRows([])
			})
		} else {
			data = utils.deleteArrayRow(data, objKey, row.id)
			setList(data)
		}
	}

	const tableColumns = [
		{
			title: 'Full Name',
			dataIndex: 'full_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'full_name')
		},
		{
			title: 'Phone number',
			dataIndex: 'phone',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'phone')
		},
		// {
		// 	title: 'Group',
		// 	dataIndex: 'group',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'group')
		// },
		{
			title: 'Birth Date',
			dataIndex: 'birth_date',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'birth_date')
		},
		// {
		// 	title: 'Last Login',
		// 	dataIndex: 'lastlogin',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'lastlogin')
		// },
		{
			title: 'Created At',
			dataIndex: 'createdAt',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'createdAt')
		},
		{
			title: 'Updated At',
			dataIndex: 'updatedAt',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'updatedAt')
		},
		{
			title: 'Info',
			dataIndex: 'info',
			render: (_, elm) => (
				<Tooltip title="View Info">
					<InfoCircleOutlined
						style={{ color: 'green', cursor: 'pointer' }}
						onClick={() => viewInfo(elm)}
					/>
				</Tooltip>
			),
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			render: (_, elm) => (
				<div className="text-right">
					<EllipsisDropdown menu={dropdownMenu(elm)} />
				</div>
			)
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value;
		// const searchArray = e.currentTarget.value ? list : ProductListData
		// const data = utils.wildCardSearch(searchArray, value)
		setSearch(value)
		// setList(data)
		setSelectedRowKeys([])
	}

	return (
		<>
			<div className="container">
				<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
					<h2 className="mb-3">All Teacher in Group</h2>

				</Flex>
			</div>
			<Card>
				<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
					<Flex className="mb-1" mobileFlex={false}>
						<div className="mr-md-3 mb-3">
							<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
						</div>
					</Flex>
					<div>
						<Button onClick={addTeacher} type="primary" icon={<PlusCircleOutlined />} block>Add Teacher</Button>
					</div>
				</Flex>
				<div className="table-responsive">
					<Table
						columns={tableColumns}
						dataSource={list.map((elem) => {
							return {
								...elem,
								full_name: elem?.studentDetails[0]?.first_name,
								phone: elem?.studentDetails[0]?.phone,
								birth_date: getDateString(elem?.studentDetails[0]?.birth_date),
								createdAt: getDateString(elem?.studentDetails[0]?.createdAt),
								updatedAt: getDateString(elem?.studentDetails[0]?.updatedAt),
							};
						})}
						rowKey='_id'
						rowSelection={{
							selectedRowKeys: selectedRowKeys,
							type: 'checkbox',
							preserveSelectedRowKeys: false,
							...rowSelection,
						}}

					/>
				</div>
			</Card>
		</>
	)
}

export default TeacherList
