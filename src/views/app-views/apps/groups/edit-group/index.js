import React from 'react'
import GroupEditForm from '../GroupEditForm';
import { useParams } from 'react-router-dom';

const EditGroup = () => {
	const params = useParams();

	return (
		<GroupEditForm mode="EDIT" param={params}/>
	)
}

export default EditGroup
