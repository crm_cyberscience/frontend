import React, { useEffect, useState } from 'react';
import { Input, Row, Col, Card, Form, Upload, DatePicker, Modal, Button, message, Select, Radio, Typography } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import StudentService from 'services/StudentService';
import GroupService from 'services/GroupService';
import dayjs from 'dayjs';
import { formatGender } from '../../teachers/TeacherInfoForm/GeneralField';
import NewPaymentModal from '../../students/StudentInfoForm/PaymentModal';
import PaymentService from 'services/PaymentService';
import { formatBalance } from '../../leads/utils';
import { getDateDayString } from 'utils/aditionalFunctions';

const { Text } = Typography;

const imageUploadProps = {
  name: 'file',
  multiple: true,
  listType: 'picture-card',
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

const GeneralField = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [group, setGroup] = useState('');
  const [money, setMoney] = useState('0');
  useEffect(() => {
    if (props.id) {
      const fetchData = async () => {
        try {
          const getGroup = await GroupService.GroupGetOne(props.id);
          const getMoney = await PaymentService.CalculateGroup({"groupId":getGroup?.data?.group[0]?._id});
          setMoney(getMoney?.data?.money[0]?.totalAmount)          
          setGroup(getGroup?.data?.group[0]);
        } catch (error) {
          console.error('Error fetching group data:', error);
        }
      };
      fetchData();
    }
  }, [props.id]);
  const handleOpenModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCreate = (values) => {
    console.log('Received values of form: ', values);
    PaymentService.PaymentCreate(values).then((data) => {
      message.success(`success`);
      console.log(`success`);
    }).catch((error) => {
      console.log(`error`)
      message.error(`error`);
      console.log(error)
    });
    // Implement your logic to handle payment creation
    setIsModalVisible(false); // Close the modal after creating payment
  };
  return (
    <Card>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24}>
          <Row gutter={ROW_GUTTER}>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Group Name"
                name="groupname"
              >
                <Text strong>{group?.name}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Teacher Name"
                name="teachername"
              >
                <Text strong>{`${group?.teacherId?.first_name} ${group?.teacherId?.last_name} ${group?.teacherId?.middle_name}`}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Course Name"
                name="coursename"
              >
                <Text strong>{group?.courseId?.name}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Start Date"
                name="startdate"
              >
                <Text strong>{getDateDayString(group?.start_date)}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="End Date"
                name="enddate"
              >
                <Text strong>{getDateDayString(group?.end_date)}</Text>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xs={24} sm={24} md={12}>
              <h4 className="mb-0">Balance</h4>
              <h1 className="font-weight-bold">{money} UZS</h1>
            </Col>
          </Row>

        </Col>
      </Row>
    </Card>
  );
};

export default GeneralField;
