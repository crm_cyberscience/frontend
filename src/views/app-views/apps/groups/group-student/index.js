import React, { useEffect, useState } from 'react'
import { Card, Table, Input, Button, Menu, Tooltip } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined, InfoCircleOutlined } from '@ant-design/icons';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate, useParams } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changeStatusStudent, getPageNumber, getPageSize, getStudents, getStudentsError, getStudentsList, getStudentsStatus } from 'store/slices/studentSlice';
import { getDateDayString, getDateString } from 'utils/aditionalFunctions';
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import GroupService from 'services/GroupService';
import GeneralField from './general-group'


const GroupList = () => {
	const { id } = useParams()
	const navigate = useNavigate();
	const [list, setList] = useState([])
	const [search, setSearch] = useState('')
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	// const dispatch = useDispatch()
	// const studentList = useSelector(getStudentsList)
	// const studentStatus = useSelector(getStudentsStatus)
	// const studentError = useSelector(getStudentsError)
	// const pageNumber = useSelector(getPageNumber)
	// const pageSize = useSelector(getPageSize)

	useEffect(() => {
		const fetchData = async () => {
			const getStudents = await GroupService.GetStudents({ groupId: id })
			// console.log(getStudents?.data?.students);
			setList(getStudents?.data?.students)
		}
		fetchData()
	}, [id])

	const dropdownMenu = row => (
		<Menu>
			<Menu.Item onClick={() => viewDetails(row)}>
				<Flex alignItems="center">
					< EditOutlined />
					<span className="ml-2">Edit</span>
				</Flex>
			</Menu.Item>
			<Menu.Item onClick={() => deleteRow(row)}>
				<Flex alignItems="center">
					<DeleteOutlined />
					<span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span>
				</Flex>
			</Menu.Item>
		</Menu>
	);

	const addStudent = () => {
		navigate(`/app/apps/students/add-student`)
	}

	const viewDetails = row => {
		navigate(`/app/apps/students/edit-student/${row?.studentId}`)
	}

	const viewInfo = (row) => {
		navigate(`/app/apps/students/info-student/${row?.studentId}`);
	};

	const deleteRow = row => {
		const objKey = 'id'
		let data = list
		if (selectedRows.length > 1) {
			selectedRows.forEach(elm => {
				data = utils.deleteArrayRow(data, objKey, elm.id)
				setList(data)
				setSelectedRows([])
			})
		} else {
			data = utils.deleteArrayRow(data, objKey, row.id)
			setList(data)
		}
	}

	const tableColumns = [
		{
			title: 'Student Name',
			dataIndex: 'student_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'student_name')
		},
		{
			title: 'Total Payment',
			dataIndex: 'totalAmount',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'totalAmount')
		},
		{
			title: 'Last Payment',
			dataIndex: 'amount',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'amount')
		},
		{
			title: 'Start Date',
			dataIndex: 'start_date',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'start_date')
		},
		{
			title: 'End Date',
			dataIndex: 'end_date',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'end_date')
		},
		// {
		// 	title: 'Comment',
		// 	dataIndex: 'comment',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'comment')
		// },
		{
			title: 'Last Method Payment',
			dataIndex: 'method',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'method')
		},
		{
			title: 'Discount',
			dataIndex: 'discount',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'discount')
		},
		// {
		// 	title: 'Group',
		// 	dataIndex: 'group',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'group')
		// },
		
		// {
		// 	title: 'Last Login',
		// 	dataIndex: 'lastlogin',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'lastlogin')
		// },
		// {
		// 	title: 'Created At',
		// 	dataIndex: 'createdAt',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'createdAt')
		// },
		// {
		// 	title: 'Updated At',
		// 	dataIndex: 'updatedAt',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'updatedAt')
		// },
		{
			title: 'Info',
			dataIndex: 'info',
			render: (_, elm) => (
				<Tooltip title="View Info">
					<InfoCircleOutlined
						style={{ color: 'green', cursor: 'pointer' }}
						onClick={() => viewInfo(elm)}
					/>
				</Tooltip>
			),
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			render: (_, elm) => (
				<div className="text-right">
					<EllipsisDropdown menu={dropdownMenu(elm)} />
				</div>
			)
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value;
		// const searchArray = e.currentTarget.value ? list : ProductListData
		// const data = utils.wildCardSearch(searchArray, value)
		setSearch(value)
		// setList(data)
		setSelectedRowKeys([])
	}

	return (
		<>
			<div className="container">
				<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
					<h2 className="mb-3">Group Info</h2>
				</Flex>
			</div>
				<GeneralField id={id}/>
			<div className="container">
				<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
					<h2 className="mb-3">All Student in Group</h2>

				</Flex>
			</div>
			<Card>
				<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
					<Flex className="mb-1" mobileFlex={false}>
						<div className="mr-md-3 mb-3">
							<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
						</div>
					</Flex>
					<div>
						<Button onClick={addStudent} type="primary" icon={<PlusCircleOutlined />} block>Add Student</Button>
					</div>
				</Flex>
				<div className="table-responsive">
					<Table
						columns={tableColumns}
						dataSource={list?.map((elem) => {
							return {
								...elem,
								student_name: [elem?.studentDetails?.first_name, elem?.studentDetails?.last_name, elem?.studentDetails?.middle_name].filter(Boolean).join(' '),
								amount: elem?.amount,
								totalAmount: elem?.totalAmount,
								start_date: getDateDayString(elem?.start_date),
								end_date: getDateDayString(elem?.end_date),
								comment: elem?.comment,
								method: elem?.method,
								discount: elem?.discount,
								studentId: elem?.studentDetails?.student_id,
								// createdAt: getDateString(elem?.studentDetails?.createdAt),
								// updatedAt: getDateString(elem?.studentDetails?.updatedAt),
							};
						})}
						rowKey='_id'
						rowSelection={{
							selectedRowKeys: selectedRowKeys,
							type: 'checkbox',
							preserveSelectedRowKeys: false,
							...rowSelection,
						}}

					/>
				</div>
			</Card>
		</>
	)
}

export default GroupList
