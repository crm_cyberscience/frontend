import React, { useState, useEffect } from 'react'
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import { Tabs, Form, Button, message } from 'antd';
import Flex from 'components/shared-components/Flex'
import GeneralField from './GeneralField'
import ProductListData from "assets/data/product-list.data.json"
import GroupService from 'services/GroupService';
import { useNavigate } from "react-router-dom"
import { changeStatus } from 'store/slices/groupSlice';
import dayjs from 'dayjs';
import { useDispatch } from 'react-redux';

const getBase64 = (img, callback) => {
	const reader = new FileReader();
	reader.addEventListener('load', () => callback(reader.result));
	reader.readAsDataURL(img);
}

const ADD = 'ADD'
const EDIT = 'EDIT'

const GroupAddForm = props => {

	const { mode = ADD, param } = props
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const [form] = Form.useForm();
	const [uploadedImg, setImage] = useState('')
	const [uploadLoading, setUploadLoading] = useState(false)
	const [submitLoading, setSubmitLoading] = useState(false)

	const [addInfo, setAddInfo] = useState(null)

	useEffect(() => {
		if (mode === EDIT) {
			console.log('is edit')
			console.log('props', props)
			const { id } = param
			const produtId = parseInt(id)
			const productData = ProductListData.filter(product => product.id === produtId)
			const product = productData[0]
			form.setFieldsValue({
				comparePrice: 0.00,
				cost: 0.00,
				taxRate: 6,
				description: 'There are many variations of passages of Lorem Ipsum available.',
				category: product.category,
				name: product.name,
				price: product.price
			});
			setImage(product.image)
		}
	}, [form, mode, param, props]);

	const navigateGroups = () => {
		navigate(`/app/apps/groups/group-list`)
	}

	const handleUploadChange = info => {
		if (info.file.status === 'uploading') {
			setUploadLoading(true)
			return;
		}
		if (info.file.status === 'done') {
			getBase64(info.file.originFileObj, imageUrl => {
				setImage(imageUrl)
				setUploadLoading(true)
			});
		}
	};

	const handleData = data => {
		const result=[]
		data.forEach(element => {
			result.push(dayjs(element).format('HH:mm'))			
		});
		return result
	};

	const handleDataCourse = data => {
		const result=[]
		data.forEach(element => {
			result.push(dayjs(element).format('YYYY-MM-DD'))			
		});
		return result
	};


	const onFinish = () => {
		// addInfo.courseId=addInfo?.course?._id
		// addInfo.teacherId=addInfo?.teacher?._id	
		// addInfo ni ma'lumotlarini api ga junatasiz 
		let data=handleData(addInfo.startOfLessonTime)
		addInfo.start_time=data[0]
		addInfo.end_time=data[1]
		data=handleDataCourse(addInfo.startOfCourseDate)
		addInfo.start_date=data[0]
		addInfo.end_date=data[1]
		console.log("addInfo");
		console.log(addInfo);
		setSubmitLoading(true)
		form.validateFields().then(values => {
			console.log("values");			
			console.log(values);			
			setTimeout(() => {
				GroupService.GroupCreate(addInfo).then((data) => {
					if (mode === ADD) {
						message.success(`Created ${values.name} to Group list`);
					}
					if (mode === EDIT) {
						message.success(`Group saved`);
					}
					dispatch(changeStatus())
					navigateGroups()
				}).catch((error) => {
					console.log(`error`)
					message.error(`error`);
					console.log(error)
				});
				setSubmitLoading(false)
			}, 1500);
		}).catch(info => {
			setSubmitLoading(false)
			console.log('info', info)
			message.error('Please enter all required field ');
		});
	};

	return (
		<>
			<Form
				layout="vertical"
				form={form}
				name="advanced_search"
				className="ant-advanced-search-form"
				initialValues={{
					heightUnit: 'cm',
					widthUnit: 'cm',
					weightUnit: 'kg'
				}}
			>
				<PageHeaderAlt className="border-bottom" overlap>
					<div className="container">
						<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
							<h2 className="mb-3">{mode === 'ADD' ? 'Add New Group' : `Edit Group`} </h2>
							<div className="mb-3">
								<Button className="mr-2">Discard</Button>
								<Button type="primary" onClick={() => onFinish()} htmlType="submit" loading={submitLoading} >
									{mode === 'ADD' ? 'Add' : `Save`}
								</Button>
							</div>
						</Flex>
					</div>
				</PageHeaderAlt>
				<div className="container">
					<Tabs
						defaultActiveKey="1"
						style={{ marginTop: 30 }}
						items={[
							{
								label: 'General',
								key: '1',
								children: <GeneralField
									uploadedImg={uploadedImg}
									uploadLoading={uploadLoading}
									handleUploadChange={handleUploadChange}
									updateDataCallBack={(val) => {setAddInfo(val)}}
								/>,
							}

						]}
					/>
				</div>
			</Form>
		</>
	)
}

export default GroupAddForm
