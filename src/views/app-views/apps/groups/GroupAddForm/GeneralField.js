import React, { useEffect, useState } from 'react';
import { Row, Col, Form, Input, AutoComplete, Select, TimePicker, DatePicker, Radio } from 'antd';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import CourseService from 'services/CourseService';
import TeacherService from 'services/TeacherService';

const { Option } = Select;
const { RangePicker: RangePickerTime } = TimePicker;
const { RangePicker: RangePickerDate } = DatePicker;
const { TextArea } = Input;
const weekdays = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

const GeneralField = (props) => {

	const [course, setCourse] = useState('')
	const [courseSearch, setCourseSearch] = useState('')
	const [courseList, setCourseList] = useState([])
	const [teacher, setTeacher] = useState('')
	const [teacherSearch, setTeacherSearch] = useState('')
	const [name, setGroupName] = useState('')
	const [teacherList, setTeacherList] = useState([])
	const [type, setType] = useState('')
	const [price, setPrice] = useState('')
	const [schedule, setSchedule] = useState([])
	const [length, setLength] = useState([])
	const [startOfCourseDate, setStartOfCourseDate] = useState('')
	const [startOfLessonTime, setStartOfLessonTime] = useState('')
	const [typeValue, setTypeValue] = useState('none');

	const { updateDataCallBack } = props

	useEffect(() => {
		// console.log({
		// 	name,
		// 	courseId: course?._id,
		// 	teacherId: teacher?._id,
		// 	price,
		// 	schedule,
		// 	length,
		// 	type: typeValue,
		// 	startOfCourseDate,
		// 	startOfLessonTime
		// });
		updateDataCallBack({
			name,
			courseId: course?._id,
			teacherId: teacher?._id,
			price,
			schedule,
			length,
			type: typeValue,
			startOfCourseDate,
			startOfLessonTime
		})
	}, [course, teacher, name, price, schedule, length, startOfCourseDate, startOfLessonTime, typeValue])

	useEffect(() => {
		const fetchData = async () => {
			const coursed = await CourseService.CourseList({ pageNumber: 1, pageSize: 8, query: course })
			setCourseList(coursed?.data?.course)
			const teacher = await TeacherService.TeacherList({ pageNumber: 1, pageSize: 8 })
			setTeacherList(teacher?.data?.teacher)
		}
		fetchData()
	}, [])

	const onChange = (value) => {
		setCourseSearch(value)
		const fetchData = async () => {
			const coursed = await CourseService.CourseList({ pageNumber: 1, pageSize: 8, query: value })
			setCourseList(coursed?.data?.course)
		}
		fetchData()
	};

	const onChangeGender = (value) => {
		setTypeValue(value?.target?.value)
	};

	const onChangeTeacher = (value) => {
		setTeacherSearch(value)
		const fetchData = async () => {
			const coursed = await TeacherService.TeacherList({ pageNumber: 1, pageSize: 8, query: value });
			setTeacherList(coursed?.data?.teacher)
		}
		fetchData()
	};

	return (
		<Row className="mt-4">
			<Col xs={24} sm={24} md={24} lg={16}>
				<Row gutter={ROW_GUTTER}>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="name"
							label="Group name"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Input onChange={(e) => { setGroupName(e.target.value) }} />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="courseId"
							label="Select Course"
							// rules={[
							// 	{
							// 		// required: true,
							// 	},
							// 	{
							// 		validator: (_, value) => {
							// 			if (!value) {
							// 				return Promise.reject('Please select a course!');
							// 			}
							// 			return Promise.resolve();
							// 		},
							// 	},
							// ]}
							// validateStatus={(_, { errors }) => {
							// 	return errors.length > 0 ? 'error' : '';
							// }}
							// help={(_, { errors }) => {
							// 	return errors.length > 0 ? 'Please select a valid course!' : '';
							// }}
						>
							<p style={{ display: "none" }} value={course.name}>{course.name}</p>
							<AutoComplete
								value={courseSearch}
								onSearch={onChange}
								onSelect={(value) => {
									const selectedOption = courseList.find((option) => option._id === value);
									setCourseSearch(selectedOption.name)
									console.log(selectedOption);
									setCourse(selectedOption);
								}}
								placeholder="Search"
							>
								{courseList.map((item) => (
									<AutoComplete.Option key={item._id} value={item._id}>
										{item.name}
									</AutoComplete.Option>
								))}
							</AutoComplete>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="teacherId"
							label="Select Teacher"
							// rules={[
							// 	{
							// 		required: true,
							// 	},
							// 	{
							// 		validator: (_, value) => {
							// 			if (!value) {
							// 				return Promise.reject('Please select a Teacher!');
							// 			}
							// 			return Promise.resolve();
							// 		},
							// 	},
							// ]}
							// validateStatus={(_, { errors }) => {
							// 	return errors.length > 0 ? 'error' : '';
							// }}
							// help={(_, { errors }) => {
							// 	return errors.length > 0 ? 'Please select a valid Teacher!' : '';
							// }}
						>
							<p style={{ display: "none" }}>{teacher.name}</p>
							<AutoComplete
								value={teacherSearch}
								onSearch={onChangeTeacher}
								onSelect={(value) => {
									const selectedOption = teacherList.find((option) => option._id === value);
									setTeacherSearch(selectedOption.first_name)
									setTeacher(selectedOption);
								}}
								placeholder="Search"
							>
								{teacherList.map((item) => (
									<AutoComplete.Option key={item._id} value={item._id}>
										{item.first_name}
									</AutoComplete.Option>
								))}
							</AutoComplete>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="price"
							label="Price"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Input onChange={(e) => { setPrice(e.target.value) }} />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="schedule"
							label="Select Days"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Select
								mode="multiple"
								style={{ width: "100%" }}
								placeholder="Please select"
								onChange={(e) => { setSchedule(e) }}
							// defaultValue={selectedDays}
							// onChange={handleChange}
							>
								{weekdays.map(day => (
									<Option key={day} value={day}>{day}</Option>
								))}
							</Select>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="length"
							label="Length"
							rules={[
								{
									required: true,
								},
							]}
						>
							<Input onChange={(e) => { setLength(e.target.value) }} />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Course start/end date"
							name="startOfCourseDate"
						>
							<RangePickerDate onChange={(e) => { setStartOfCourseDate(e) }} />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Lesson start/end time"
							name="startOfLessonTime"
						>
							<RangePickerTime format={'HH:mm'} onChange={(e) => { setStartOfLessonTime(e) }} />
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							label="Type"
							name="type"
						>
							<Radio.Group onChange={onChangeGender} value={typeValue}>
								<Radio value={'express'}>Express</Radio>
								<Radio value={'none'}>None</Radio>
							</Radio.Group>
						</Form.Item>
					</Col>
				</Row>
			</Col>

		</Row>
	);
};

export default GeneralField;
