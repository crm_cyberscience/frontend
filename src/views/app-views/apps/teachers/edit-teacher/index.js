import React from 'react'
import TeacherEditForm from '../TeacherEditForm';
import { useParams } from 'react-router-dom';

const EditProduct = () => {
	const params = useParams();

	return (
		<TeacherEditForm mode="EDIT" param={params}/>
	)
}

export default EditProduct
