import React, { useEffect, useState } from 'react';
import { Input, Row, Col, Card, Form, Upload, DatePicker, Button, message, Select, Radio } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';

const imageUploadProps = {
  name: 'file',
  multiple: true,
  listType: 'picture-card',
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

const GeneralField = ({teacher}) => {

  const [teacherObj,setTeacherObj]=useState(null)

  useEffect(() => {
    console.log(teacher, "<-props?.teacher")
    // setTeacherObj(props?.teacher || {});
  }, [teacher]);  
  
  const [genderValue, setGenderValue] = useState(1);

  const onChange = (e) => {
    console.log('radio checked', e.target.value);
    setGenderValue(e.target.value);
  };

  if(teacher){
    return (
      <Row>
        <Col xs={24} sm={24} md={24} lg={16}>
          <Row gutter={ROW_GUTTER}>
            <Col xs={24} sm={24} md={8}>
            {(teacher?.first_name !== undefined)&&
              <Form.Item
                label="First Name"
                name="firstname"
                // initialValue={teacher?.first_name}
                initialValue={teacher?.first_name}
                rules={[
                  {
                    required: true,
                    message: 'Please input your name!',
                  },
                ]}
              >
                {/* <p onClick={()=>{console.log(teacher?.first_name)}}>log to console</p> */}
                <Input />
              </Form.Item>}
            </Col>
            <Col xs={24} sm={24} md={8}>
              {(teacher?.last_name!== undefined)&&<Form.Item
                label="Last Name"
                name="lastname"
                initialValue={teacher?.last_name}
                rules={[
                  {
                    required: true,
                    message: 'Please input your name!',
                  },
                ]}
              >
                <Input />
              </Form.Item>}
            </Col>
            <Col xs={24} sm={24} md={8}>
              {(teacher?.middle_name!== undefined)&&<Form.Item
                label="Middle Name"
                name="middlename"
                initialValue={teacher?.middle_name}
                rules={[
                  {
                    required: true,
                    message: 'Please input your name!',
                  },
                ]}
              >
                <Input />
              </Form.Item>}
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Phone Number"
                name="phoneNumber"
                rules={[
                  {
                    required: true,
                    message: 'Please input your phone number!',
                  },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                name="password"
                label="Password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password',
                  },
                ]}
              >
                <Input.Password autoComplete='new-password' prefix={<LockOutlined />} />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Additional Phone Number"
                name="additionalNumber"
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Date of Birth"
                name="dateOfBirth"
              >
                <DatePicker className="w-100" />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Gender"
                name="gender"
              >
                <Radio.Group onChange={onChange} value={genderValue}>
                  <Radio value={1}>Male</Radio>
                  <Radio value={2}>Female</Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          {/* <Button type="primary" htmlType="submit">
            Submit
          </Button> */}
        </Col>
      </Row>
    );
  }else{
    return <p>Loading...</p>
  }
  
};

export default GeneralField;
