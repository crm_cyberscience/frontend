import React, { useState, useEffect } from 'react'
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import { Tabs, Form, Button, message } from 'antd';
import Flex from 'components/shared-components/Flex'
import GeneralField from './GeneralField'
import ProductListData from "assets/data/product-list.data.json"
import TeacherService from 'services/TeacherService';

const getBase64 = (img, callback) => {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

const ADD = 'ADD'
const EDIT = 'EDIT'

const TeacherEditForm = props => {

	const { mode = EDIT, param } = props
	
	const [form] = Form.useForm();
	const [uploadedImg, setImage] = useState('')
	const [teacher, setTeacher] = useState({})
	const [test, setTest] = useState('')
	const [uploadLoading, setUploadLoading] = useState(false)
	const [submitLoading, setSubmitLoading] = useState(false)

	const { id } = param
	
	useEffect(() => {
    	if(mode === EDIT) {
			// console.log('is edit')
			// console.log('props', props)
			
			// const produtId = parseInt(id)
			const fetchData = async () => {
				const coursed = await TeacherService.TeacherGetOne(id)
				if (coursed?.data?.code==200) {
					console.log(coursed?.data)
					setTeacher(coursed?.data?.student[0])				
					setTest('test')
				}else{
					console.log('set zero');
					setTeacher([])
				}
			}
			fetchData()
			// const productData = ProductListData.filter( product => product.id === produtId)
			// const product = productData[0]
			// form.setFieldsValue({
			// 	comparePrice: 0.00,
			// 	cost: 0.00,
			// 	taxRate: 6,
			// 	description: 'There are many variations of passages of Lorem Ipsum available.',
			// 	category: product.category,
			// 	name: product.name,
			// 	price: product.price
			// });
			// setImage(product.image)
		}
  	}, [id]);

	const handleUploadChange = info => {
		if (info.file.status === 'uploading') {
			setUploadLoading(true)
			return;
		}
		if (info.file.status === 'done') {
			getBase64(info.file.originFileObj, imageUrl =>{
				setImage(imageUrl)
				setUploadLoading(true)
			});
		}
	};

	const onFinish = () => {
		setSubmitLoading(true)
		form.validateFields().then(values => {
			setTimeout(() => {
				setSubmitLoading(false)
				if(mode === ADD) {
					message.success(`Created ${values.name} to product list`);
				}
				if(mode === EDIT) {
					message.success(`Product saved`);
				}
			}, 1500);
		}).catch(info => {
			setSubmitLoading(false)
			console.log('info', info)
			message.error('Please enter all required field ');
		});
	};

	return (
		<>
		{<p>{teacher.first_name}</p>}
			{teacher&&<Form
				layout="vertical"
				form={form}
				name="advanced_search"
				className="ant-advanced-search-form"
				// initialValues={{
				// 	firstname: teacher.first_name,
				// 	lastname: 'cm',
				// 	middlename: 'kg',
				// 	phoneNumber: 'kg',
				// 	password: '',
				// 	dateOfBirth: '',
				// 	gender: '',
				// 	additionalNumber: 'kg'
				// }}
			>
				<PageHeaderAlt className="border-bottom" overlap>
					<div className="container">
						<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
							<h2 className="mb-3">{mode === 'ADD'? 'Add New Teacher' : `Edit Teacher`} </h2>
							<div className="mb-3">
								<Button className="mr-2">Discard</Button>
								<Button type="primary" onClick={() => onFinish()} htmlType="submit" loading={submitLoading} >
									{mode === 'ADD'? 'Add' : `Save`}
								</Button>
							</div>
						</Flex>
					</div>
				</PageHeaderAlt>
				<div className="container">
					<Tabs 
						defaultActiveKey="1" 
						style={{marginTop: 30}}
						items={[
							{
								label: 'General',
								key: '1',
								children: <GeneralField 
									uploadedImg={uploadedImg} 
									uploadLoading={uploadLoading}
									teacher={teacher}
									handleUploadChange={handleUploadChange}
								/>,
							}
							
						]}
					/>
				</div>
			</Form>}
		</>
	)
}

export default TeacherEditForm
