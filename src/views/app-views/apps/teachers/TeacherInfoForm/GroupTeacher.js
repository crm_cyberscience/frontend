import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Input, Button, Badge, Menu, message } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { EyeOutlined, DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined } from '@ant-design/icons';
import AvatarStatus from 'components/shared-components/AvatarStatus';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changeStatus, getGroupError, getGroupList, getGroupPageNumber, getGroupPageSize, getGroups, getGroupStatus } from 'store/slices/groupSlice';
import GroupService from 'services/GroupService';
import { getDateString } from 'utils/aditionalFunctions';

const { Option } = Select
const categories = ['Cloths', 'Bags', 'Shoes', 'Watches', 'Devices']

const GroupList = (param) => {
	const navigate = useNavigate();
	const [list, setList] = useState(ProductListData)
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	const dispatch = useDispatch()
	const groupList = useSelector(getGroupList)
	const groupStatus = useSelector(getGroupStatus)
	const groupError = useSelector(getGroupError)
	const pageNumber = useSelector(getGroupPageNumber)
	const pageSize = useSelector(getGroupPageSize)

	useEffect(() => {
		// if (groupStatus === 'idle') {
		// 	dispatch(getGroups({ pageNumber: pageNumber, pageSize: pageSize }))
		// }
	}, [groupStatus, dispatch])

	useEffect(() => {
		setList(groupList);
	}, [groupList]);



	const addGroup = () => {
		navigate(`/app/apps/teachers/add-to-group/${param.id}`)
	}

	const tableColumns = [
		{
			title: 'Name',
			dataIndex: 'name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'name')
		},
		{
			title: 'Teacher Name',
			dataIndex: 'teacher_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'price')
		},
		{
			title: 'Course name',
			dataIndex: 'course_name',
		},
		{
			title: 'Created At',
			dataIndex: 'created_at',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'created_at')
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value
		const searchArray = e.currentTarget.value ? list : ProductListData
		const data = utils.wildCardSearch(searchArray, value)
		setList(data)
		setSelectedRowKeys([])
	}

	const handleShowCategory = value => {
		if (value !== 'All') {
			const key = 'category'
			const data = utils.filterArray(ProductListData, key, value)
			setList(data)
		} else {
			setList(ProductListData)
		}
	}

	return (
		<Card>
			<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
				<Flex className="mb-1" mobileFlex={false}>
					<div className="mr-md-3 mb-3">
						<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
					</div>
					{/* <div className="mb-3">
						<Select
							defaultValue="All"
							className="w-100"
							style={{ minWidth: 180 }}
							onChange={handleShowCategory}
							placeholder="Category"
						>
							<Option value="All">All</Option>
							{
								categories.map(elm => (
									<Option key={elm} value={elm}>{elm}</Option>
								))
							}
						</Select>
					</div> */}
				</Flex>
				<div>
					<Button onClick={addGroup} type="primary" icon={<PlusCircleOutlined />} block>Add Group</Button>
				</div>
			</Flex>
			<div className="table-responsive">
				<Table
					columns={tableColumns}
					dataSource={list.map((elem) => {
						return {
							...elem,
							teacher_name: elem?.teacherId?.first_name,
							course_name: elem?.courseId?.name,
							start_date: getDateString(elem.start_date),
							end_date: getDateString(elem.end_date),
							created_at: getDateString(elem.created_at),
							updated_at: getDateString(elem.updated_at),
						};
					})}
					rowKey='_id'
					rowSelection={{
						selectedRowKeys: selectedRowKeys,
						type: 'checkbox',
						preserveSelectedRowKeys: false,
						...rowSelection,
					}}
				/>
			</div>
		</Card>
	)
}

export default GroupList
