import React, { useEffect, useState } from 'react';
import { Input, Row, Col, Card, Form, Upload, DatePicker, Button, message, Select, Radio, Typography } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import StudentService from 'services/StudentService';
import TeacherService from 'services/TeacherService';
import dayjs from 'dayjs';

const { Text } = Typography;

export const formatGender = (e) => {
  if (e === 'male') {
    return 'Erkak'
  } else {
    return 'Ayol'
  }
};

const imageUploadProps = {
  name: 'file',
  multiple: true,
  listType: 'picture-card',
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

const GeneralField = (props) => {
  const [genderValue, setGenderValue] = useState(1);
  const [teacher, setTeacher] = useState(false);

  useEffect(() => {
    const fetch_data = async () => {
      const user = await TeacherService.TeacherGetOne(props.id)
      setTeacher(user?.data?.student[0])
    }
    fetch_data()
  }, [])


  return (
    <Card>
      <Row>
      <Col xs={24} sm={24} md={24} lg={24}>
        <Row gutter={ROW_GUTTER}>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="First Name:"
              name="firstname"
            >
              <Text strong>{teacher?.first_name}</Text>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Last Name:"
              name="last_name"
            >
              <Text strong>{teacher?.last_name}</Text>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Middle Name:"
              name="middle_name"
            >
              <Text strong>{teacher?.middle_name}</Text>
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Phone Number:"
              name="phone"
            >
              <Text strong>{teacher?.phone}</Text>
            </Form.Item>
          </Col>
          {/* <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Additional Phone Number:"
              name="additionalNumber"
            >
              <Text strong>{teacher?.phone}</Text>
            </Form.Item>
          </Col> */}
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Date of Birth:"
              name="birth_date"
            >
              <DatePicker className="w-50" defaultValue={dayjs(teacher?.birth_date)} />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Gender:"
              name="gender"
            >
              <Text strong>{formatGender(teacher?.gender)}</Text>
            </Form.Item>
          </Col>
        </Row>
      </Col>
    </Row>
    </Card>
  );
};

export default GeneralField;
