import React from 'react'
import TeacherInfoForm from '../TeacherInfoForm';
import { useParams } from 'react-router-dom';

const InfoTeacher = () => {
	const params = useParams();

	return (
		<TeacherInfoForm mode="INFO" param={params}/>
	)
}

export default InfoTeacher
