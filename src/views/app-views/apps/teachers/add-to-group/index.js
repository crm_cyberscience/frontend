import React from 'react';
import GroupAddForm from '../AddToGroupForm';

const AddGroup = () => {
	return (
		<GroupAddForm mode="ADD"/>
	)
}

export default AddGroup
