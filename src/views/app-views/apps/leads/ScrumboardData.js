const y = new Date().getFullYear();

export const memberIds = [
	'eileenHorton-1153',
	'terranceMoreno-1322',
	'ronVargas7653',
	'lukeCook4721',
	'joyceFreeman1384',
	'samanthaPhillips8493',
	'taraFletcher1263',
	'frederickAdams6532',
	'carolynHanson7953',
	'brittanyHale3683',
	'lloydObrien1564',
	'gabriellaMay2850',
	'leeWheeler1941',
	'gailBarnes7615',
	'ellaRobinson1093'
]

export const membersDetail = [
  {
    id: "eileenHorton-1153",
    name: "Eileen Horton",
    email: "eileen_h@hotmail.com",
    img: "/img/avatars/thumb-1.jpg"
  },
  {
    id: "terranceMoreno-1322",
    name: "Terrance Moreno",
    email: "",
    img: "/img/avatars/thumb-2.jpg"
  },
  {
    id: "ronVargas7653",
    name: "Ron Vargas",
    email: "ronnie_vergas@infotech.io",
    img: "/img/avatars/thumb-3.jpg"
  },
  {
    id: "lukeCook4721",
    name: "Luke Cook",
    email: "cookie_lukie@hotmail.com",
    img: "/img/avatars/thumb-4.jpg"
  },
  {
    id: "joyceFreeman1384",
    name: "Joyce Freeman",
    email: "joyce991@infotech.io",
    img: "/img/avatars/thumb-5.jpg"
  },
  {
    id: "samanthaPhillips8493",
    name: "Samantha Phillips",
    email: "samanthaphil@infotech.io",
    img: "/img/avatars/thumb-6.jpg"
  },
  {
    id: "taraFletcher1263",
    name: "Tara Fletcher",
    email: "taratarara@imaze.edu.du",
    img: "/img/avatars/thumb-7.jpg"
  },
  {
    id: "frederickAdams6532",
    name: "Frederick Adams",
    email: "iamfred@imaze.infotech.io",
    img: "/img/avatars/thumb-8.jpg"
  },
  {
    id: "carolynHanson7953",
    name: "Carolyn Hanson",
    email: "carolyn_h@gmail.com",
    img: "/img/avatars/thumb-9.jpg"
  },
  {
    id: "brittanyHale3683",
    name: "Brittany Hale",
    email: "brittany1134@gmail.com",
    img: "/img/avatars/thumb-10.jpg"
  },
  {
    id: "lloydObrien1564",
    name: "Lloyd Obrien",
    email: "handsome-obrien@hotmail.com",
    img: "/img/avatars/thumb-11.jpg"
  },
  {
    id: "gabriellaMay2850",
    name: "Gabriella May",
    email: "maymaymay12@infotech.io",
    img: "/img/avatars/thumb-12.jpg"
  },
  {
    id: "leeWheeler1941",
    name: "Lee Wheeler",
    email: "",
    img: "/img/avatars/thumb-13.jpg"
  },
  {
    id: "gailBarnes7615",
    name: "Gail Barnes",
    email: "gailby0116@infotech.io",
    img: "/img/avatars/thumb-14.jpg"
  },
  {
    id: "ellaRobinson1093",
    name: "Ella Robinson",
    email: "ella_robinson@infotech.io",
    img: "/img/avatars/thumb-15.jpg"
  }
]

export const labels = [
	{
		color: 'blue',
		label: 'Task'
	},
	{
		color: 'gold',
		label: 'Bug'
	},
	{
		color: 'red',
		label: 'Live issue'
	},
	{
		color: 'cyan',
		label: 'Low priority'
	}
];  

const toBeProcessCards = [
	{
		id: 'zb7zxtjctd',
		name: 'Unable to upload file',
		description: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		'dueDate': new Date(y, 7, 5)
	},
	{
		id: '7qgsduurxt',
		name: 'Table data incorrect',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 6, 11)
	},
	{
		id: 'wtwgpz6csc',
		name: 'Fix broken UI',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		'dueDate': new Date(y, 7, 5)
	},
]

const processingCards = [
	{
		id: 'ywejrdr3rn',
		name: 'Fix dashboard layout',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 3, 17)
	},
	{
		id: 'tkBXWJGwkr',
		name: 'New design',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: null
	},
	{
		id: 'VQgUDrYJYH',
		name: 'Improve user experiences',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 4, 20)
	}
]

const submittedCards = [
	{
		id: 'jzjn95g3v4',
		name: 'Update node environment',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: null
	},
	{
		id: 'ZFQDPmscwA',
		name: 'Remove user data',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: null
	},
]

const completedCards = [
	{
		id: 'yhjk5679xr',
		name: 'Ready to test',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 3, 4)
	},
	{
		id: 'UMgvapYVXm',
		name: 'Slow API connection',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 7, 19)
	},
	{
		id: 'uRZNVsCmDW',
		name: 'Login failed',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 4, 6)
	},
	{
		id: 'PBSGmhVgvS',
		name: 'Locale incorrect',
		description: '',
		cover: '',
		members: [],
		labels: [],
		attachments: [],
		comments: [],
		dueDate: new Date(y, 7, 13)
	}
]

export const scrumboardData = {
	'To be processed': [...toBeProcessCards],
	'Processing': [...processingCards],
	'Submitted': [...submittedCards],
	'Completed': [...completedCards]
}