import React, { useState, useEffect, useContext } from 'react'
import { 
	Modal, 
	Form, 
	Input, 
	Button,
	Divider
} from 'antd'
import { modalModeTypes } from './utils'
import { scrumboardData } from './ScrumboardData'
import dayjs from 'dayjs'
import { ScrumboardContext } from './ScrumboardContext'
import { List, Typography } from "antd";

const getModalTitle = type => {
	switch (type) {
		case modalModeTypes(0):
			return 'New card';
		case modalModeTypes(2):
			return 'New board';
		default:
			return;
	}
} 

const AddCardForm = ({onSubmit}) => {
	return (
		<Form layout="inline" name="add-card-ref" onFinish={onSubmit}>
			<Form.Item name="cardTitle" label="Card Title">
				<Input autoComplete="off" />
			</Form.Item>
			<Form.Item name="cardTitle" label="Card Title">
				<Input autoComplete="off" />
			</Form.Item>
			<Form.Item>
				<Button type="primary" htmlType="submit">Add</Button>
			</Form.Item>
		</Form>
	)
}

const data = [
	"Racing car sprays burning fuel into crowd.",
	"Japanese princess to wed commoner.",
	"Australian walks 100km after outback crash.",
	"Man charged over missing wedding girl.",
	"Los Angeles battles huge wildfires."
  ];

const UpdateCardForm = ({onSubmit, cardData, listId}) => {
	const context = useContext(ScrumboardContext)

	const [attachmentsList, setAttachmentsList] = useState(cardData.attachments)
	const [commentsList, setCommentsList] = useState(cardData.comments)
	const [cover, setCover] = useState(cardData.cover)

	useEffect(() => {
		setAttachmentsList(cardData.attachments);
		setCommentsList(cardData.comments);
		setCover(cardData.cover)
	}, [cardData.attachments, cardData.comments, cardData.cover]);

	const initialValues = {
		name: cardData?.name,
		members: cardData?.members,
		dueDate: cardData?.dueDate ? dayjs(cardData.dueDate) : '',
		labels: cardData?.labels,
		description: cardData?.description,
	}

	const submitUpdate = values => {
		//agar uzgartirish kiritiladigan buldsa ishlatiladi
		let updatedValue = values
		updatedValue.attachments = attachmentsList
		updatedValue.comments = commentsList
		updatedValue.cover = cover
		updatedValue.id = cardData.id
		onSubmit(updatedValue)
	}

	return (
		<Form name="edit-card-ref" layout="vertical" onFinish={submitUpdate} initialValues={initialValues}>
			<Form.Item className="mb-3">
				<p>Board: <span className="font-weight-semibold">{listId}</span></p>
			</Form.Item>
			<Divider className="mt-0"/>
			<List
          header={null}
          footer={null}
          bordered
          dataSource={data}
          renderItem={item => (
            <List.Item>
              <Typography.Text mark></Typography.Text> {item}
            </List.Item>
          )}
        />
			<div className="d-flex">
			</div>
		</Form>
	)	
}

const AddBoardForm = ({onSubmit}) => {
	return(
		<Form layout="inline" name="add-board-ref" onFinish={onSubmit}>
			<Form.Item 
				name="boardTitle"
				label="Board Title"
				rules={[() => ({
						validator(rule, value) {
							if(scrumboardData[value]) {
								return Promise.reject('Board already exist!');
							}
							return Promise.resolve();
						},
					}),
				]}
			>
				<Input autoComplete="off" />
			</Form.Item>
			<Form.Item>
				<Button type="primary" htmlType="submit">Add</Button>
			</Form.Item>
		</Form>
	)
}

const ModalForm = ({ open, modalMode, cardData, listId, onClose, onModalSubmit }) => {

	const showClosable = modalMode === modalModeTypes(1) ? false : true
	const modalWidth = modalMode === modalModeTypes(1) ? 800 : 425;

	const submit = (values, mode) => {
		onModalSubmit(values, mode)
		onClose()
	};

	return (
		<Modal
			title={getModalTitle(modalMode)}
			open={open}
			closable={showClosable}
			footer={null}
			width={modalWidth}
			style={modalMode === modalModeTypes(1)? {top: 20} : null}
			destroyOnClose
			onCancel={() => onClose()}
		>
			<div style={modalMode === modalModeTypes(1)? {maxHeight: '85vh', overflowY: 'auto', overflowX: 'hidden'} : null}>
				<div className={modalMode === modalModeTypes(1)? 'mr-2 ml-2' : null}>
					{ 
						(() => {
							switch(modalMode) {
								case modalModeTypes(0):
									return <AddCardForm onSubmit={values => submit(values, modalModeTypes(0))}/>;
								case modalModeTypes(1):
									return (
										<UpdateCardForm 
											cardData={cardData} 
											listId={listId} 
											onSubmit={values => submit(values, modalModeTypes(1))}
										/>
									);
								case modalModeTypes(2):
									return <AddBoardForm onSubmit={values => submit(values, modalModeTypes(2))}/>;
								default:
									return null;
							}
						})()
					}
				</div>
			</div>
		</Modal>
	)
}

export default ModalForm
