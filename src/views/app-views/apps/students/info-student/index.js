import React from 'react'
import StudentInfoForm from '../StudentInfoForm';
import { useParams } from 'react-router-dom';

const InfoTeacher = () => {
	const params = useParams();

	return (
		<StudentInfoForm mode="INFO" param={params}/>
	)
}

export default InfoTeacher
