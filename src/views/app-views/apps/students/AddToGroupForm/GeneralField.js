import React, { useEffect, useState } from 'react';
import { Row, Col, Form, Select, Input, AutoComplete, Typography } from 'antd';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import CourseService from 'services/CourseService';
import StudentService from 'services/StudentService';
import GroupService from 'services/GroupService';

const { Text } = Typography

const { Option } = Select;

const GeneralField = (props) => {
	const [course, setCourse] = useState('')
	const [groupList, setGroupList] = useState([])
	const [student, setStudent] = useState(false);

	useEffect(() => {
		const fetchData = async () => {
			const coursed = await GroupService.GroupList({ pageNumber: 1, pageSize: 8, query: course })
			if (coursed?.data?.code==200) {
				setGroupList(coursed?.data?.group)				
			}else{
				console.log('set zero');
				setGroupList([])
			}
		}
		fetchData()
	}, [course])

	useEffect(() => {
		const fetch_student = async () => {
			const user = await StudentService.StudentGetOne(props.id)
			setStudent(user?.data?.student[0])
		}
		fetch_student()
	}, [])

	const onChange = (value) => {
		setCourse(value)
		const fetchData = async () => {
			const coursed = await GroupService.GroupList({ pageNumber: 1, pageSize: 8, query: course })
			if (coursed?.data?.code===200) {
				setGroupList(coursed?.data?.group)				
				// setGroupList([])
			} else {
				setGroupList([])
			}
		}
		fetchData()
	};

	return (
		<Row className="mt-4">
			<Col xs={24} sm={24} md={24} lg={16}>
				<Row gutter={ROW_GUTTER}>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="first_name"
							label="First name"
						>
							<Text strong>{student?.first_name}</Text>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="last_name"
							label="Last name"
						>
							<Text strong>{student?.last_name}</Text>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="groupId"
							label="Select Group"
							rules={[
								{
									required: true,
								},
							]}
						>
							<AutoComplete
								value={course}
								onSearch={onChange}
								placeholder="Search"
							>
								{groupList.map((item) => (
									<AutoComplete.Option key={item.id} value={item._id}>
										{item.name}
									</AutoComplete.Option>
								))}
							</AutoComplete>
						</Form.Item>
					</Col>
					<Col xs={24} sm={24} md={8}>
						<Form.Item
							name="discount"
							label="Discount"
						>
							<Input placeholder='Discount'></Input>
						</Form.Item>
					</Col>
				</Row>
			</Col>

		</Row>
	);
};

export default GeneralField;
