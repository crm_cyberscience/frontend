import React from 'react';
import StudentAddForm from '../StudentAddForm';

const AddStudent = () => {
	return (
		<StudentAddForm mode="ADD"/>
	)
}

export default AddStudent
