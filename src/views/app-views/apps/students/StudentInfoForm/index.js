import React, { useState, useEffect } from 'react'
import PageHeaderAlt from 'components/layout-components/PageHeaderAlt'
import { Tabs, Form, Button, message } from 'antd';
import Flex from 'components/shared-components/Flex'
import GeneralField from './GeneralField'
import PaymentField from './PaymentField'
import GroupStudent from './GroupStudent'
import ProductListData from "assets/data/product-list.data.json"

const getBase64 = (img, callback) => {
	const reader = new FileReader();
	reader.addEventListener('load', () => callback(reader.result));
	reader.readAsDataURL(img);
}

const INFO = 'INFO'

const StudentInfoForm = props => {

	const { mode = INFO, param } = props

	const [form] = Form.useForm();
	const [uploadedImg, setImage] = useState('')
	const [uploadLoading, setUploadLoading] = useState(false)



	return (
		<>
			<Form
				layout="vertical"
				form={form}
				name="advanced_search"
				className="ant-advanced-search-form"
				initialValues={{
					heightUnit: 'cm',
					widthUnit: 'cm',
					weightUnit: 'kg'
				}}
			>
				<PageHeaderAlt className="border-bottom" overlap>
					<div className="container">
						<Flex className="py-2" mobileFlex={false} justifyContent="space-between" alignItems="center">
							<h2 className="mb-3">{mode === 'INFO' ? 'About Student' : `Edit Student`} </h2>

						</Flex>
					</div>
				</PageHeaderAlt>
				<div className="container">
					<Tabs
						defaultActiveKey="1"
						style={{ marginTop: 30 }}
						items={[
							{
								label: 'General',
								key: '1',
								children: <GeneralField
									uploadedImg={uploadedImg}
									uploadLoading={uploadLoading}
									id={param?.id}
								// handleUploadChange={handleUploadChange}
								/>,
							},
							{
								label: 'Payments',
								key: '3',
								children: <PaymentField
									id={param?.id}
								/>,
							},
							{
								label: 'Groups',
								key: '4',
								children: <GroupStudent
									id={param?.id}
								/>,
							},
						]}
					/>
				</div>
			</Form>
		</>
	)
}

export default StudentInfoForm
