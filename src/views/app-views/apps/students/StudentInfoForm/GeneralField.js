import React, { useEffect, useState } from 'react';
import { Input, Row, Col, Card, Form, Upload, DatePicker, Modal, Button, message, Select, Radio, Typography } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import StudentService from 'services/StudentService';
import dayjs from 'dayjs';
import { formatGender } from '../../teachers/TeacherInfoForm/GeneralField';
import NewPaymentModal from './PaymentModal';
import PaymentService from 'services/PaymentService';
import { formatBalance } from '../../leads/utils';

const { Text } = Typography;

const imageUploadProps = {
  name: 'file',
  multiple: true,
  listType: 'picture-card',
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

const GeneralField = (props) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [student, setStudent] = useState('');

  useEffect(() => {
    const fetch_data = async () => {
      const user = await StudentService.StudentGetOne(props.id)
      setStudent(user?.data?.student[0])
      console.log(user?.data?.student[0])
    }
    fetch_data()
  }, [])

  const handleOpenModal = () => {
    setIsModalVisible(true);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleCreate = (values) => {
    console.log('Received values of form: ', values);
    PaymentService.PaymentCreate(values).then((data) => {
      message.success(`success`);
      console.log(`success`);
    }).catch((error) => {
      console.log(`error`)
      message.error(`error`);
      console.log(error)
    });
    // Implement your logic to handle payment creation
    setIsModalVisible(false); // Close the modal after creating payment
  };

  return (
    <Card>
      <Row>
        <Col xs={24} sm={24} md={24} lg={24}>
          <Row gutter={ROW_GUTTER}>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="First Name"
                name="firstname"
              >
                <Text strong>{student?.first_name}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Last Name"
                name="lastname"
              >
                <Text strong>{student?.last_name}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Middle Name"
                name="middlename"
              >
                <Text strong>{student?.middle_name}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Phone Number"
                name="phoneNumber"
              >
                <Text strong>{student?.phone}</Text>
              </Form.Item>
            </Col>
            {/* <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Additional Phone Number"
              name="additionalNumber"
            >
              <Text strong>{teacher?.phone}</Text>
            </Form.Item>
          </Col> */}
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Date of Birth"
                name="dateOfBirth"
              >
                <DatePicker className="w-50" defaultValue={dayjs(student?.birth_date)} />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Gender"
                name="gender"
              >
                <Text strong>{formatGender(student?.gender)}</Text>
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Status"
                name="status"
              >
                <Text strong>Active</Text>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col xs={24} sm={24} md={12}>
              <h4 className="mb-0">Balance</h4>
              <h1 className="font-weight-bold">{formatBalance(student?.balance)} UZS</h1>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <div className="mt-3 text-right">
                <Button type="primary" onClick={handleOpenModal}>New Payment</Button>
                <NewPaymentModal
                  visible={isModalVisible}
                  onCreate={handleCreate}
                  onCancel={handleCancel}
                />
              </div>
            </Col>
          </Row>

        </Col>
      </Row>
    </Card>
  );
};

export default GeneralField;
