import React, { Component, useEffect, useState } from 'react'
import { Table, Button, Tooltip, Form, Modal, Input, Row, Col, Select, InputNumber, Card, message, Menu } from 'antd';
import { UserOutlined, DollarOutlined } from '@ant-design/icons';
import utils from 'utils';
import NumberFormat from 'react-number-format';
import {
  recentOrderData,
} from './DefaultDashboardData';
import Flex from 'components/shared-components/Flex'
import { DeleteOutlined } from '@ant-design/icons';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import PaymentService from 'services/PaymentService';
import { getDateString } from 'utils/aditionalFunctions';
import { useParams } from 'react-router-dom';
import StudentService from 'services/StudentService';
import NewPaymentModal from './PaymentModal';

const { Option } = Select;

const { TextArea } = Input;

const PaymentForm = ({ visible, onCreate, onCancel }) => {
  const params = useParams();
  const [user, setUser] = useState('');
  const [addInfo, setAddInfo] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      const student = await StudentService.StudentGetOne(params.id)
      console.log(student?.data?.student)
      setUser(student?.data?.student[0])
    }
    fetchData()
  }, [])

  const [form] = Form.useForm();
  return (
    <Modal
      title="New Payment"
      visible={visible}
      okText="Payment"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            form.resetFields();
            onCreate(values);
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
        form={form}
        name="createPayment"
        layout="vertical"
        className='mt-3'
      >
        <Form.Item
          label="Student name"
          name="studentName"
          rules={[
            {
              required: true,
              message: 'Please enter student name!'
            }
          ]}
        >
          <Input suffix={<UserOutlined />} placeholder="Student name" />
        </Form.Item>
        <Form.Item
          label="Deposit"
          name="deposit"
          rules={[
            {
              required: true,
              message: 'Please entere deposit!'
            }
          ]}
        >
          <InputNumber suffix={<DollarOutlined />} placeholder="Deposit" style={{ width: "100%" }} ></InputNumber>
        </Form.Item>
        <Form.Item
          name="type"
          label="Payment Type"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select
            style={{ width: "100%" }}
            placeholder="Please select"
          >
            <Option value="humo">HUMO</Option>
            <Option value="uzcart">UZCARD</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Comment"
          name="comment"
        >
          <TextArea
            // value={value}
            // onChange={this.onChange}
            placeholder="Comment ..."
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
      </Form>
    </Modal>
  )
}

const deleteRow = row => {
  PaymentService.Delete(row?._id)
    .then((data) => {
      message.success(`Deleted ${row?._id} from payment`);
    })
    .catch((error) => {
      console.log(`error`);
      message.error(`error`);
      console.log(error);
    });
};

const dropdownMenu = row => (
  <Menu>
    <Menu.Item onClick={() => { deleteRow(row) }}>
      <Flex alignItems="center">
        <DeleteOutlined />
        <span className="ml-2">Delete</span>
      </Flex>
    </Menu.Item>
  </Menu>
);

const tableColumns = [
  {
    title: 'Student',
    dataIndex: 'student_name',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'student_name')
  },
  {
    title: 'Course Name',
    dataIndex: 'course_name',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'course_name')
  },
  {
    title: 'Group Name',
    dataIndex: 'group_name',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'group_name')
  },
  // {
  //   title: 'Student',
  //   dataIndex: 'student_name',
  //   sorter: (a, b) => utils.antdTableSorter(a, b, 'student_name')
  // },
  {
    title: 'Total Amount',
    dataIndex: 'amount',
    render: (_, record) => (
      <span className="font-weight-semibold">
        <NumberFormat
          displayType={'text'}
          value={(Math.round(record.amount * 100) / 100).toFixed(2)}
          suffix={' UZS'}
          thousandSeparator={true}
        />
      </span>
    ),
    sorter: (a, b) => utils.antdTableSorter(a, b, 'amount')
  },
  {
    title: 'Start Date',
    dataIndex: 'start_date',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'start_date')
  },
  {
    title: 'End Date',
    dataIndex: 'end_date',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'end_date')
  },
  {
    title: 'Discount',
    dataIndex: 'discount',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'discount')
  },
  {
    title: 'Method Pay',
    dataIndex: 'method',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'method')
  },
  // {
  //   title: 'Admin',
  //   dataIndex: 'adminId',
  //   sorter: (a, b) => utils.antdTableSorter(a, b, 'adminId')
  // },
  {
    title: 'Comment',
    dataIndex: 'comment'
  },
  // {
  //   title: 'Creator',
  //   dataIndex: 'who',
  // },
  // {
  //   title: 'Created At',
  //   dataIndex: 'created_at',
  // },
  // {
  //   title: 'Updated At',
  //   dataIndex: 'updated_at',
  // },
  {
    title: 'Actions',
    dataIndex: 'actions',
    render: (_, elm) => (
      <div className="text-right">
        <EllipsisDropdown menu={dropdownMenu(elm)} />
      </div>
    )
  }
]

const RecentPayments = (props) => {

  const [list, setList] = useState([])
  // console.log(props?.id);

  useEffect(() => {
    const fetchData = async () => {
      const payment = await PaymentService.PaymentList({ pageNumber: 1, pageSize: 8, studentID: props?.id });
      setList(payment?.data?.payments)
      console.log(payment?.data?.payments);
    }
    fetchData()
  }, [])

  return (
    <Card title="Recent Transaction">
      <Table
        pagination={false}
        columns={tableColumns}
        dataSource={list.map(elem => {
          return {
            ...elem,
            student_name: [elem?.student_id?.first_name, elem?.student_id?.last_name, elem?.student_id?.middle_name].filter(Boolean).join(' '),
            course_name: elem?.course_id?.name,
            group_name: elem?.groupId?.name,
            amount: elem?.amount,
            comment: elem?.comment,
            method: elem?.method,
            start_date: getDateString(elem?.start_date),
            end_date: getDateString(elem?.end_date),
            // created_at: getDateString(elem?.created_at),
            // updated_at: getDateString(elem?.updated_at),
            // schedule: elem.schedule.join(",")
          }
        })}
        rowKey='_id'
      />
    </Card>
  )
}

export class Billing extends Component {

  state = {
    selectedRowKeys: ["card-1"], // Check here to configure the default column
    creditCards: [
      {
        key: 'card-1',
        cardType: 'Visa',
        cardTypeImg: '/img/others/img-8.png',
        cardNumber: '•••• •••• •••• 7260',
        exp: '06/22'
      },
      {
        key: 'card-2',
        cardType: 'Master',
        cardTypeImg: '/img/others/img-9.png',
        cardNumber: '•••• •••• •••• 1272',
        exp: '04/21'
      }
    ],
    modalVisible: false,
    newCreditCardInfo: {
      cardHolderName: '',
      cardNumber: '',
      exp: '06/22'
    },
    addInfo: ''
  };

  onSelectChange = (selectedRowKeys, selectedRows) => {
    this.setState({ selectedRowKeys });
  };

  showModal = () => {
    this.setState({
      modalVisible: true,
    });
  };

  closeModal = () => {
    this.setState({
      modalVisible: false,
    });
  }

  addCard = values => {

  };

  handleCreate = (values) => {
    PaymentService.PaymentCreate(values).then((data) => {
      message.success(`success`);
      console.log(`success`);
    }).catch((error) => {
      console.log(`error`)
      console.log(error)
      message.error(`error`);
    });
    // Implement your logic to handle payment creation
    this.closeModal() // Close the modal after creating payment
  };

  handleUpdate = (val) => { this.setState({ addInfo: val }) }

  render() {
    console.log(this?.props?.id);
    const { selectedRowKeys, creditCards, modalVisible } = this.state;
    const rowSelection = {
      selectedRowKeys,
      type: 'radio',
      onChange: this.onSelectChange,
    };

    const locale = {
      emptyText: (
        <div className="text-center my-4">
          <img src="/img/others/img-7.png" alt="Add credit card" style={{ maxWidth: '90px' }} />
          <h3 className="mt-3 font-weight-light">Please add a credit card!</h3>
        </div>
      )
    };

    return (
      <>
        <Row gutter={16}>
          <Col xs={24} sm={24} md={24} lg={24}>
            <RecentPayments id={this?.props?.id} />
          </Col>
        </Row>
        <div className="mt-3 text-right">
          <Button type="primary" onClick={this.showModal}>New Payment</Button>
        </div>
        <NewPaymentModal
          visible={modalVisible}
          onCreate={this.handleCreate}
          onCancel={this.closeModal}
          updateDataCallBack={this.handleUpdate}
        />
        {/* <PaymentForm visible={modalVisible} onCreate={this.addCard} onCancel={this.closeModal} /> */}
      </>
    )
  }
}

export default Billing
