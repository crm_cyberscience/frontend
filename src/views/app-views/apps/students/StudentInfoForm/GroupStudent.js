import React, { useEffect, useState } from 'react'
import { Card, Table, Select, Input, Button, Switch, message } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { SearchOutlined, PlusCircleOutlined } from '@ant-design/icons';
import Flex from 'components/shared-components/Flex'
import { useNavigate } from "react-router-dom";
import utils from 'utils'
import { useDispatch } from 'react-redux';
import GroupService from 'services/GroupService';
import { getDateString } from 'utils/aditionalFunctions';

const { Option } = Select
const categories = ['Cloths', 'Bags', 'Shoes', 'Watches', 'Devices']

const GroupList = (param) => {
	const navigate = useNavigate();
	const [list, setList] = useState([])
	const [idStudent, setId] = useState(param?.id)
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	useEffect(() => {
		const fetchDataGroup= async ()=>{
			const groups=await GroupService.GetByStudent({studentId:idStudent});
			setList(groups?.data?.group)
		}
		fetchDataGroup()
	}, [])

	const addGroup = () => {
		navigate(`/app/apps/students/add-to-group/${param.id}`)
	}

	const handleStatusChange = async (checked, record) => {
		try {
			const updatedStatus = checked ? 'active' : 'inactive';
			// await dispatch(changeStatus({ id: record._id, status: updatedStatus }));
			await GroupService.ChangeGroupSubsSts({studentId: record.student_id,groupId: record._id,status:updatedStatus })
			message.success(`Status changed to ${updatedStatus}`);
			// Optionally, refresh the list
			const groups=await GroupService.GetByStudent({studentId:idStudent});
			setList(groups?.data?.group)
		} catch (error) {
			message.error('Failed to change status');
		}
	};

	const tableColumns = [
		{
			title: 'Group Name',
			dataIndex: 'group_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'group_name')
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value
		const searchArray = e.currentTarget.value ? list : ProductListData
		const data = utils.wildCardSearch(searchArray, value)
		setList(data)
		setSelectedRowKeys([])
	}

	const handleShowCategory = value => {
		if (value !== 'All') {
			const key = 'category'
			const data = utils.filterArray(ProductListData, key, value)
			setList(data)
		} else {
			setList(ProductListData)
		}
	}

	return (
		<Card>
			<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
				<Flex className="mb-1" mobileFlex={false}>
					<div className="mr-md-3 mb-3">
						<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
					</div>
					{/* <div className="mb-3">
						<Select
							defaultValue="All"
							className="w-100"
							style={{ minWidth: 180 }}
							onChange={handleShowCategory}
							placeholder="Category"
						>
							<Option value="All">All</Option>
							{
								categories.map(elm => (
									<Option key={elm} value={elm}>{elm}</Option>
								))
							}
						</Select>
					</div> */}
				</Flex>
				<div>
					<Button onClick={addGroup} type="primary" icon={<PlusCircleOutlined />} block>Add Group</Button>
				</div>
			</Flex>
			<div className="table-responsive">
				<Table
					columns={tableColumns}
					dataSource={list.map((elem) => {
						return {
							...elem,
							group_name: elem?.groupId?.name
							// status: elem?.status,
							// start_date: getDateString(elem?.start_date),
							// end_date: getDateString(elem?.end_date),
							// created_at: getDateString(elem?.created_at),
							// updated_at: getDateString(elem?.updated_at),
						};
					})}
					rowKey='_id'
					rowSelection={{
						selectedRowKeys: selectedRowKeys,
						type: 'checkbox',
						preserveSelectedRowKeys: false,
						...rowSelection,
					}}
				/>
			</div>
		</Card>
	)
}

export default GroupList
