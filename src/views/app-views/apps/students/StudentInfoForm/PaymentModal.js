import React, { useEffect, useState } from 'react';
import { Modal, Form, Input, Button, Select, InputNumber, DatePicker, AutoComplete } from 'antd';
import { UserOutlined, DollarOutlined } from '@ant-design/icons';
import { useParams } from 'react-router-dom';
import StudentService from 'services/StudentService';
import AuthService from 'services/AuthService';
import CourseService from 'services/CourseService';
import GroupService from 'services/GroupService';
import dayjs from 'dayjs';




const { TextArea } = Input;
const { Option } = Select;
const { RangePicker: RangePickerDate } = DatePicker;

const NewPaymentModal = ({ visible, onCreate, onCancel, updateDataCallBack }) => {
  const params = useParams();
  const [form] = Form.useForm();
  const [user, setUser] = useState('');
  const [course, setCourse] = useState('')
  const [courseSearch, setCourseSearch] = useState('')
  const [courseList, setCourseList] = useState([])
  const [admin, setAdmin] = useState('');
  const [money, setMoney] = useState('');
  const [type, settype] = useState('');
  const [comment, setComment] = useState('');
  const [startOfCourseDate, setStartOfCourseDate] = useState('')

  useEffect(() => {
    const fetchData = async () => {
      const coursed = await GroupService.GroupList({ pageNumber: 1, pageSize: 8, query: course })
      setCourseList(coursed?.data?.group)
    }
    fetchData()
  }, [])
  useEffect(() => {
    const fetchData = async () => {
      const student = await StudentService.StudentGetOne(params.id)
      setUser(student?.data?.student[0])
      const data = await AuthService.me()
      setAdmin(data.data.user[0])
    }
    fetchData()
  }, [])
  const onChange = (value) => {
    setCourseSearch(value)
    const fetchData = async () => {
      const coursed = await GroupService.GroupList({ pageNumber: 1, pageSize: 8, query: value })
      setCourseList(coursed?.data?.course)
    }
    fetchData()
  };
  const handleDataCourse = data => {
		const result=[]
		data.forEach(element => {
			result.push(dayjs(element).format('YYYY-MM-DD'))			
		});
		return result
	};

  return (
    <Modal
      title="New Payment"
      visible={visible}
      okText="Payment"
      onCancel={onCancel}
      onOk={() => {
        form
          .validateFields()
          .then(values => {
            values.studentId = user?._id
            values.adminId = admin?._id
            values.groupId = course?._id
            let data = handleDataCourse(values.startOfCourseDate)
            values.startDate = data[0]
            values.endDate = data[1]
            // console.log("values means that");
            // console.log(values);

            onCreate(values);
            // form.resetFields();
          })
          .catch(info => {
            console.log('Validate Failed:', info);
          });
      }}
    >
      <Form
        form={form}
        name="createPayment"
        layout="vertical"
        className='mt-3'
      >
        <Form.Item
          label="Student name"
          name="studentName"
          value={user?.first_name}
        >
          <Input suffix={<UserOutlined />} defaultValue={user?.first_name} disabled />
        </Form.Item>
        <Form.Item
          name="courseId"
          label="Select Course"
        // rules={[
        // 	{
        // 		// required: true,
        // 	},
        // 	{
        // 		validator: (_, value) => {
        // 			if (!value) {
        // 				return Promise.reject('Please select a course!');
        // 			}
        // 			return Promise.resolve();
        // 		},
        // 	},
        // ]}
        // validateStatus={(_, { errors }) => {
        // 	return errors.length > 0 ? 'error' : '';
        // }}
        // help={(_, { errors }) => {
        // 	return errors.length > 0 ? 'Please select a valid course!' : '';
        // }}
        >
          <p style={{ display: "none" }} value={course.name}>{course.name}</p>
          <AutoComplete
            value={courseSearch}
            onSearch={onChange}
            onSelect={(value) => {
              const selectedOption = courseList.find((option) => option._id === value);
              setCourseSearch(selectedOption.name)
              // console.log(selectedOption);
              setCourse(selectedOption);
            }}
            placeholder="Search"
          >
            {courseList?.map((item) => (
              <AutoComplete.Option key={item._id} value={item._id}>
                {item.name}
              </AutoComplete.Option>
            ))}
          </AutoComplete>
        </Form.Item>
        <Form.Item
          label="Course start/end date"
          name="startOfCourseDate"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <RangePickerDate onChange={(e) => { setStartOfCourseDate(e) }} />
        </Form.Item>
        <Form.Item
          label="Deposit"
          name="money"
          rules={[
            {
              required: true,
              message: 'Please enter deposit!'
            }
          ]}
        >
          {/* <InputNumber placeholder="Deposit" style={{ width: "100%" }} ></InputNumber> */}
          <InputNumber placeholder="Deposit" style={{ width: "100%" }} onChange={(e) => { setMoney(e) }} />
        </Form.Item>
        <Form.Item
          label="Discount"
          name="discount"
          rules={[
            {
              required: true,
              message: 'Please enter discount!'
            }
          ]}
        >
          <InputNumber placeholder="Discount" style={{ width: "100%" }} ></InputNumber>
        </Form.Item>
        <Form.Item
          name="type"
          label="Payment Type"
          rules={[
            {
              required: true,
            },
          ]}
        >
          <Select
            style={{ width: "100%" }}
            placeholder="Please select"
          >
            <Option value="humo">HUMO</Option>
            <Option value="uzcart">UZCARD</Option>
            <Option value="naqd">Naqd</Option>
            <Option value="boshqa">Boshqa</Option>
          </Select>
        </Form.Item>
        <Form.Item
          label="Comment"
          name="comment"
        >
          <TextArea
            // value={value}
            // onChange={this.onChange}
            placeholder="Comment ..."
            autoSize={{ minRows: 3, maxRows: 5 }}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default NewPaymentModal;
