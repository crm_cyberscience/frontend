import React, { useEffect, useState } from 'react'
import { Card, Table, Input, Button, Menu, Tooltip, message } from 'antd';
import ProductListData from "assets/data/product-list.data.json"
import { DeleteOutlined, SearchOutlined, PlusCircleOutlined, EditOutlined, InfoCircleOutlined } from '@ant-design/icons';
import EllipsisDropdown from 'components/shared-components/EllipsisDropdown';
import Flex from 'components/shared-components/Flex'
import { useNavigate } from "react-router-dom";
import utils from 'utils'
import { useDispatch, useSelector } from 'react-redux';
import { changePageNumber, changeStatusStudent, getPageNumber, getPageSize, getStudents, getStudentsError, getStudentsList, getStudentsStatus, getTotalPage } from 'store/slices/studentSlice';
import { getDateDayString, getDateString } from 'utils/aditionalFunctions';
import StudentService from 'services/StudentService';

const StudentList = () => {
	const navigate = useNavigate();
	const [list, setList] = useState(ProductListData)
	const [search, setSearch] = useState('')
	const [selectedRows, setSelectedRows] = useState([])
	const [selectedRowKeys, setSelectedRowKeys] = useState([])

	const dispatch = useDispatch()
	const studentList = useSelector(getStudentsList)
	const studentStatus = useSelector(getStudentsStatus)
	const studentError = useSelector(getStudentsError)
	const pageNumber = useSelector(getPageNumber)
	const totalPage = useSelector(getTotalPage)
	const pageSize = useSelector(getPageSize)

	useEffect(() => {
		if (studentStatus === 'idle') {
			dispatch(getStudents({ pageNumber: pageNumber, pageSize: pageSize }))
		}
	}, [studentStatus, dispatch, pageNumber])

	useEffect(() => {
		dispatch(getStudents({ pageNumber: pageNumber, pageSize: pageSize, query: search }))
		dispatch(changeStatusStudent())
	}, [search])

	const dropdownMenu = row => (
		<Menu>
			<Menu.Item onClick={() => viewDetails(row)}>
				<Flex alignItems="center">
					< EditOutlined />
					<span className="ml-2">Edit</span>
				</Flex>
			</Menu.Item>
			<Menu.Item onClick={() => deleteRow(row)}>
				<Flex alignItems="center">
					<DeleteOutlined />
					<span className="ml-2">{selectedRows.length > 0 ? `Delete (${selectedRows.length})` : 'Delete'}</span>
				</Flex>
			</Menu.Item>
		</Menu>
	);

	const addStudent = () => {
		navigate(`/app/apps/students/add-student`)
	}

	const viewDetails = row => {
		navigate(`/app/apps/students/edit-student/${row._id}`)
	}

	const viewInfo = (row) => {
		navigate(`/app/apps/students/info-student/${row._id}`);
	};

	const deleteRow = row => {
		if (selectedRows.length > 1) {
			selectedRows.forEach((elm) => {
				StudentService.StudentDelete(elm._id)
					.then((data) => {
						message.success(`Deleted ${elm.first_name} from teacher`);
						dispatch(changeStatusStudent());
					})
					.catch((error) => {
						console.log(`error`);
						message.error(`error`);
						console.log(error);
					});
			});
		} else {
			StudentService.StudentDelete(row._id)
				.then((data) => {
					message.success(`Deleted ${row.first_name} from teacher`);
					dispatch(changeStatusStudent());
				})
				.catch((error) => {
					console.log(`error`);
					message.error(`error`);
					console.log(error);
				});
		}
	};

	const tableColumns = [
		{
			title: 'Full Name',
			dataIndex: 'full_name',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'full_name')
		},
		{
			title: 'Phone number',
			dataIndex: 'phone',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'phone')
		},
		// {
		// 	title: 'Group',
		// 	dataIndex: 'group',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'group')
		// },
		{
			title: 'Birth Date',
			dataIndex: 'birth_date',
			sorter: (a, b) => utils.antdTableSorter(a, b, 'birth_date')
		},
		// {
		// 	title: 'Last Login',
		// 	dataIndex: 'lastlogin',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'lastlogin')
		// },
		// {
		// 	title: 'Created At',
		// 	dataIndex: 'createdAt',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'createdAt')
		// },
		// {
		// 	title: 'Updated At',
		// 	dataIndex: 'updatedAt',
		// 	sorter: (a, b) => utils.antdTableSorter(a, b, 'updatedAt')
		// },
		{
			title: 'Info',
			dataIndex: 'info',
			render: (_, elm) => (
				<Tooltip title="View Info">
					<InfoCircleOutlined
						style={{ color: 'green', cursor: 'pointer' }}
						onClick={() => viewInfo(elm)}
					/>
				</Tooltip>
			),
		},
		{
			title: 'Actions',
			dataIndex: 'actions',
			render: (_, elm) => (
				<div className="text-right">
					<EllipsisDropdown menu={dropdownMenu(elm)} />
				</div>
			)
		}
	];

	const rowSelection = {
		onChange: (key, rows) => {
			setSelectedRows(rows)
			setSelectedRowKeys(key)
		}
	};

	const onSearch = e => {
		const value = e.currentTarget.value;
		// const searchArray = e.currentTarget.value ? list : ProductListData
		// const data = utils.wildCardSearch(searchArray, value)
		setSearch(value)
		// setList(data)
		setSelectedRowKeys([])
	}

	return (
		<Card>
			<Flex alignItems="center" justifyContent="space-between" mobileFlex={false}>
				<Flex className="mb-1" mobileFlex={false}>
					<div className="mr-md-3 mb-3">
						<Input placeholder="Search" prefix={<SearchOutlined />} onChange={e => onSearch(e)} />
					</div>
				</Flex>
				<div>
					<Button onClick={addStudent} type="primary" icon={<PlusCircleOutlined />} block>Add Student</Button>
				</div>
			</Flex>
			<div className="table-responsive">
				<Table
					columns={tableColumns}
					dataSource={studentList.map((elem) => {
						return {
							...elem,
							birth_date: getDateDayString(elem.birth_date),
							createdAt: getDateString(elem.createdAt),
							updatedAt: getDateString(elem.updatedAt),
						};
					})}
					rowKey='_id'
					rowSelection={{
						selectedRowKeys: selectedRowKeys,
						type: 'checkbox',
						preserveSelectedRowKeys: false,
						...rowSelection,
					}}
					pagination={{
						pageSize: pageSize,
						total: totalPage * pageSize,
						onChange: (page, size) => {
							dispatch(changePageNumber(page));
						},
					}}
				/>
			</div>
		</Card>
	)
}

export default StudentList
