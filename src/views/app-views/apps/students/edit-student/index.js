import React from 'react'
import StudentEditForm from '../StudentEditForm';
import { useParams } from 'react-router-dom';

const EditStudent = () => {
	const params = useParams();

	return (
		<StudentEditForm mode="EDIT" param={params}/>
	)
}

export default EditStudent
