import React, { useState } from 'react';
import { Input, Row, Col, Card, Form, Upload, DatePicker, Button, message, Select, Radio } from 'antd';
import { LockOutlined } from '@ant-design/icons';
import { ROW_GUTTER } from 'constants/ThemeConstant';

const imageUploadProps = {
  name: 'file',
  multiple: true,
  listType: 'picture-card',
  showUploadList: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
};

const beforeUpload = (file) => {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isJpgOrPng) {
    message.error('You can only upload JPG/PNG file!');
  }
  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isJpgOrPng && isLt2M;
};

const GeneralField = (props) => {
  const [genderValue, setGenderValue] = useState(1);

  const onChange = (e) => {
    setGenderValue(e.target.value);
  };

  return (
    <Row>
      <Col xs={24} sm={24} md={24} lg={16}>
        <Row gutter={ROW_GUTTER}>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="First Name"
              name="first_name"
              rules={[
                {
                  required: true,
                  message: 'Please input your name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Last Name"
              name="last_name"
              rules={[
                {
                  required: true,
                  message: 'Please input your name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Middle Name"
              name="middle_name"
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Phone Number"
              name="phone"
              rules={[
                {
                  required: true,
                  message: 'Please input your phone number!',
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              name="password"
              label="Password"
              rules={[
                {
                  required: true,
                  message: 'Please input your password',
                },
              ]}
            >
              <Input.Password autoComplete='new-password' prefix={<LockOutlined />} />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Additional Phone Number"
              name="additionalNumber"
            >
              <Input />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Date of Birth"
              name="birth_date"
              // rules={[
              //   {
              //     required: true,
              //     message: 'Please choose your birth date!',
              //   },
              // ]}
            >
              <DatePicker className="w-100" />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={8}>
            <Form.Item
              label="Gender"
              name="gender"
              rules={[
                {
                  required: true,
                  message: 'Please choose your gender!',
                },
              ]}
            >
              <Radio.Group onChange={onChange} value={genderValue}>
                <Radio value={'male'}>Male</Radio>
                <Radio value={'female'}>Female</Radio>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        {/* <Button type="primary" htmlType="submit">
          Submit
        </Button> */}
      </Col>
    </Row>
  );
};

export default GeneralField;
