import React, { useState } from "react";
import { Row, Col, Table, List, Tag, Select, Form, Input, Button, DatePicker } from 'antd';
import StatisticWidget from 'components/shared-components/StatisticWidget';
import ChartWidget from 'components/shared-components/ChartWidget';
import Card from 'components/shared-components/Card';
import dayjs from 'dayjs';
import { DATE_FORMAT_DD_MM_YYYY } from 'constants/DateConstant';
import NumberFormat from 'react-number-format';
import { ROW_GUTTER } from 'constants/ThemeConstant';
import {
  VisitorChartData,
  AnnualStatisticData,
  recentOrderData,
  pagesViewData
} from './DefaultDashboardData';
import utils from 'utils';
import { useSelector } from 'react-redux';
import { useEffect } from "react";
import PaymentService from "services/PaymentService";
import { getDateString } from "utils/aditionalFunctions";
import { formatBalance } from "views/app-views/apps/leads/utils";

const { Option } = Select;
const { RangePicker: RangePickerDate } = DatePicker;

const tableColumns = [
  {
    title: 'Student',
    dataIndex: 'student_name',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'student_name')
  },
  // {
  //   title: 'Type',
  //   dataIndex: 'type',
  //   // render: (_, record) => (
  //   //   <span>{dayjs.unix(record.date).format(DATE_FORMAT_DD_MM_YYYY)}</span>
  //   // ),
  //   sorter: (a, b) => utils.antdTableSorter(a, b, 'type')
  // },
  {
    title: 'Total Sum',
    dataIndex: 'amount',
    render: (_, record) => (
      <span className="font-weight-semibold">
        <NumberFormat
          displayType={'text'}
          value={(Math.round(record.amount * 100) / 100).toFixed(2)}
          suffix={' UZS'}
          thousandSeparator={true}
        />
      </span>
    ),
    sorter: (a, b) => utils.antdTableSorter(a, b, 'amount')
  },
  {
    title: 'Method Pay',
    dataIndex: 'type',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'type')
  },
  {
    title: 'Admin',
    dataIndex: 'adminId',
    sorter: (a, b) => utils.antdTableSorter(a, b, 'adminId')
  },
  {
    title: 'Comment',
    dataIndex: 'comment'
  },
  {
    title: 'Creator',
    dataIndex: 'who',
  },
  {
    title: 'Created At',
    dataIndex: 'created_at',
  },
  {
    title: 'Updated At',
    dataIndex: 'updated_at',
  }
]

const RecentOrder = () => {
  const [list, setList] = useState([])

  useEffect(() => {
    const fetchData = async () => {
      const payment = await PaymentService.PaymentList({ pageNumber: 1, pageSize: 8 });
      setList(payment?.data?.payments)
    }
    fetchData()
  }, [])


  return <Card title="Recent Transaction">
    <Table
      pagination={false}
      columns={tableColumns}
      dataSource={list.map(elem => {
        return {
          ...elem,
          student_name: elem?.studentId?.first_name,
          amount: elem?.money,
          type: elem?.type,
          adminId: elem?.adminId?.first_name,
          comment: elem?.comment,
          who: elem?.who,
          created_at: getDateString(elem?.created_at),
          updated_at: getDateString(elem?.updated_at),
          // schedule: elem.schedule.join(",")
        }
      })}
      rowKey='id'
    />
  </Card>
}

export const DefaultDashboard = () => {
  const [visitorChartData] = useState(VisitorChartData);
  const [annualStatisticData] = useState(AnnualStatisticData);
  const [revenue, setRevenue] = useState('');
  const [revenueAll, setRevenueAll] = useState('');

  const { direction } = useSelector(state => state.theme)

  useEffect(() => {
    const fetchData = async () => {
      const payment = await PaymentService.CalculateMonth()
      setRevenue(payment?.data?.money*(-1))
      const month = await PaymentService.CalculateMonthSystem()
      setRevenueAll(month?.data?.money)
    }
    fetchData()
  }, [])

  function getCurrentMonthDateRange() {
    const now = new Date();
    const year = now.getFullYear();
    const month = String(now.getMonth() + 1).padStart(2, '0'); // getMonth() is zero-based, so we add 1
    const firstDay = `01.${month}.${year}`;
    const lastDay = new Date(year, now.getMonth() + 1, 0).getDate(); // get last day of the month
    const formattedLastDay = `${String(lastDay).padStart(2, '0')}.${month}.${year}`;
    
    return `${firstDay}-${formattedLastDay}`;
}

  return (
    <>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={18}>
          <Row gutter={16}>
            {/* {
              annualStatisticData.map((elm, i) => (
                <Col xs={24} sm={24} md={24} lg={24} xl={12} key={i}>
                  <StatisticWidget
                    title={elm.title}
                    value={elm.value}
                    status={elm.status}
                    subtitle={elm.subtitle}
                    data={elm.data}
                  />
                </Col>
              ))
            } */}
            <Col xs={24} sm={24} md={24} lg={24} xl={12}>
              <StatisticWidget
                title={'Oylik umumiy foyda'}
                value={formatBalance(revenue)}
                status={-11}
                subtitle={'elm.subtitle'}
                data={getCurrentMonthDateRange()}
              />
            </Col>
            <Col xs={24} sm={24} md={24} lg={24} xl={12}>
              <StatisticWidget
                title={'Oylik Kurslar foyda'}
                value={formatBalance(revenueAll)}
                status={-11}
                subtitle={'elm.subtitle'}
                data={getCurrentMonthDateRange()}
              />
            </Col>
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <ChartWidget
                title="Payments"
                series={visitorChartData.series}
                xAxis={visitorChartData.categories}
                height={'400px'}
                direction={direction}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} xxl={6}>
          <Card title="All Payments">
            <List
              itemLayout="horizontal"
              dataSource={pagesViewData}
              renderItem={item => (
                <List.Item>
                  <div className="d-flex align-items-center justify-content-between w-100">
                    <div>
                      <h4 className="mb-0">{item.title}</h4>
                      {/* <span className="text-muted">{item.url}</span> */}
                    </div>
                    <div>
                      <Tag color="blue">
                        <span className="font-weight-bold">
                          <NumberFormat value={item.amount} thousandSeparator={true} displayType="text" ></NumberFormat> UZS
                        </span>
                      </Tag>
                    </div>
                  </div>
                </List.Item>
              )}
            />
          </Card>
        </Col>
      </Row>
      <Row className="mt-4">
        <Col xs={24} sm={24} md={24} lg={24}>
          <Row gutter={ROW_GUTTER}>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Date From/To"
                name="date"
              >
                <RangePickerDate />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Name or Phone"
                name="name"
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Select Group"
                name="select group"
              >
                <Select />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Course"
                name="course"
              >
                <Select />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Teacher"
                name="teacher"
              >
                <Select />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Method Pay"
                name="method_pay"
              >
                <Select />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Sum"
                name="sum"
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Staff Name"
                name="staff_name"
              >
                <Input />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Form.Item
                label="Created Date From/To"
                name="created_date"
              >
                <RangePickerDate />
              </Form.Item>
            </Col>
            <Col xs={24} sm={24} md={8}>
              <Button type="primary">
                Filter
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row gutter={16} className="mt-5">
        <Col xs={24} sm={24} md={24} lg={24}>
          <RecentOrder />
        </Col>
      </Row>

    </>
  )
}


export default DefaultDashboard;
