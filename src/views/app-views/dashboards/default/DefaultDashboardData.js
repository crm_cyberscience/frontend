export const VisitorChartData = {
	series: [
		{
			name: "Session Duration",
			data: [45, 52, 38, 24, 33, 26, 21, 20, 6, 8, 15, 10]
		},
		{
			name: "Page Views",
			data: [35, 41, 62, 42, 13, 18, 29, 37, 36, 51, 32, 35]
		}
	],
	categories: [
		'01 Jan',
		'02 Jan',
		'03 Jan',
		'04 Jan',
		'05 Jan',
		'06 Jan',
		'07 Jan',
		'08 Jan',
		'09 Jan',
		'10 Jan',
		'11 Jan',
		'12 Jan'
	]
}

export const AnnualStatisticData = [
	{
		title: 'Total Revenue',
		value: '2 000 000',
		status: -11.4,
		subtitle: `Compare to last month (2024)`,
		data: '01.05.2024-01.03.2024'
	},
	{
		title: 'Total Net Profit',
		value: '5 000 000',
		status: 8.2,
		subtitle: `Compare to last month (2024)`,
		data: '01.05.2024-01.03.2024'
	}
]

export const recentOrderData = [
	{
		id: '#5331',
		name: 'Eileen Horton',
		date: 1573430400,
		amount: 677,
		paymentStatus: 'Paid',
		orderStatus: 'Ready'
	},
	{
		id: '#5328',
		name: 'Terrance Moreno',
		date: 1572393600,
		amount: 1328.35,
		paymentStatus: 'Paid',
		orderStatus: 'Ready'
	},
	{
		id: '#5321',
		name: 'Ron Vargas',
		date: 1593949805,
		amount: 629,
		paymentStatus: 'Paid',
		orderStatus: 'Shipped'
	},
	{
		id: '#5287',
		name: 'Luke Cook',
		date: 1579132800,
		amount: 25.9,
		paymentStatus: 'Paid',
		orderStatus: 'Shipped'
	},
	{
		id: '#5351',
		name: 'Joyce Freeman',
		date: 1591286400,
		amount: 817.5,
		paymentStatus: 'Pending',
		orderStatus: 'Ready'
	},
];

export const pagesViewData = [
	{
    title: 'Cash',
    amount: 7616
  },
  {
    title: 'UZCARD',
    amount: 6923
  },
  {
    title: 'Bank account',
    amount: 5228
  },
  {
    title: 'Payme',
    amount: 3512
  },
  {
    title: 'Click',
    amount: 1707
  },
  {
    title: 'Uzum',
    amount: 1707
  },
  {
    title: 'Humo',
    amount: 1707
  },
  {
    title: 'Total Revenue',
    amount: 1707
  }
]