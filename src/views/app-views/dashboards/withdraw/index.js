import React, { useState } from "react";
import { Row, Col, Table, List, Tag } from 'antd';
import StatisticWidget from 'components/shared-components/StatisticWidget';
import ChartWidget from 'components/shared-components/ChartWidget';
import AvatarStatus from 'components/shared-components/AvatarStatus';
import Card from 'components/shared-components/Card';
import Flex from 'components/shared-components/Flex';
import dayjs from 'dayjs';
import { DATE_FORMAT_DD_MM_YYYY } from 'constants/DateConstant';
import NumberFormat from 'react-number-format';
import {
  VisitorChartData,
  AnnualStatisticData,
  recentOrderData,
  pagesViewData
} from './DefaultDashboardData';
import utils from 'utils';
import { useSelector } from 'react-redux';


const tableColumns = [
  {
    title: 'ID',
    dataIndex: 'id'
  },
  {
    title: 'Product',
    dataIndex: 'name',
    render: (_, record) => (
      <Flex>
        <AvatarStatus size={30} src={record.image} name={record.name} />
      </Flex>
    ),
    sorter: (a, b) => utils.antdTableSorter(a, b, 'name')
  },
  {
    title: 'Date',
    dataIndex: 'date',
    render: (_, record) => (
      <span>{dayjs.unix(record.date).format(DATE_FORMAT_DD_MM_YYYY)}</span>
    ),
    sorter: (a, b) => utils.antdTableSorter(a, b, 'date')
  },
  {
    title: 'Total',
    dataIndex: 'amount',
    render: (_, record) => (
      <span className="font-weight-semibold">
        <NumberFormat
          displayType={'text'}
          value={(Math.round(record.amount * 100) / 100).toFixed(2)}
          prefix={'$'}
          thousandSeparator={true}
        />
      </span>
    ),
    sorter: (a, b) => utils.antdTableSorter(a, b, 'amount')
  }
]

const RecentOrder = () => (
  <Card title="Recent Order">
    <Table
      pagination={false}
      columns={tableColumns}
      dataSource={recentOrderData}
      rowKey='id'
    />
  </Card>
)

export const DefaultDashboard = () => {
  const [visitorChartData] = useState(VisitorChartData);
  const [annualStatisticData] = useState(AnnualStatisticData);
  const { direction } = useSelector(state => state.theme)

  return (
    <>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={18}>
          <Row gutter={16}>
            {
              annualStatisticData.map((elm, i) => (
                <Col xs={24} sm={24} md={24} lg={24} xl={12} key={i}>
                  <StatisticWidget
                    title={elm.title}
                    value={elm.value}
                    status={elm.status}
                    subtitle={elm.subtitle}
                    data={elm.data}
                  />
                </Col>
              ))
            }
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <ChartWidget
                title="Payments"
                series={visitorChartData.series}
                xAxis={visitorChartData.categories}
                height={'400px'}
                direction={direction}
              />
            </Col>
          </Row>
        </Col>
        <Col xs={24} sm={24} md={24} lg={12} xxl={6}>
          <Card title="All Payments">
            <List
              itemLayout="horizontal"
              dataSource={pagesViewData}
              renderItem={item => (
                <List.Item>
                  <div className="d-flex align-items-center justify-content-between w-100">
                    <div>
                      <h4 className="mb-0">{item.title}</h4>
                      {/* <span className="text-muted">{item.url}</span> */}
                    </div>
                    <div>
                      <Tag color="blue">
                        <span className="font-weight-bold">
                          <NumberFormat value={item.amount} thousandSeparator={true} displayType="text" ></NumberFormat> UZS
                        </span>
                      </Tag>
                    </div>
                  </div>
                </List.Item>
              )}
            />
          </Card>
        </Col>
      </Row>
      <Row gutter={16}>
        <Col xs={24} sm={24} md={24} lg={24}>
          <RecentOrder />
        </Col>
      </Row>

    </>
  )
}


export default DefaultDashboard;
