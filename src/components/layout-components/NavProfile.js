import React, { useEffect, useState } from 'react';
import { Dropdown, Avatar } from 'antd';
import { useDispatch } from 'react-redux'
import {
	EditOutlined,
	SettingOutlined,
	ShopOutlined,
	QuestionCircleOutlined,
	LogoutOutlined
} from '@ant-design/icons';
import NavItem from './NavItem';
import Flex from 'components/shared-components/Flex';
import { signOut } from 'store/slices/authSlice';
import styled from '@emotion/styled';
import { FONT_WEIGHT, MEDIA_QUERIES, SPACER, FONT_SIZES } from 'constants/ThemeConstant'
import { getStorage } from 'utils/storage';
import AuthService from 'services/AuthService';

const Icon = styled.div(() => ({
	fontSize: FONT_SIZES.LG
}))

const Profile = styled.div(() => ({
	display: 'flex',
	alignItems: 'center'
}))

const UserInfo = styled('div')`
	padding-left: ${SPACER[2]};

	@media ${MEDIA_QUERIES.MOBILE} {
		display: none
	}
`

const Name = styled.div(() => ({
	fontWeight: FONT_WEIGHT.SEMIBOLD
}))

const Title = styled.span(() => ({
	opacity: 0.8
}))

const MenuItem = (props) => (
	<Flex as="a" href={props.path} alignItems="center" gap={SPACER[2]}>
		<Icon>{props.icon}</Icon>
		<span>{props.label}</span>
	</Flex>
)

const MenuItemSignOut = (props) => {

	const dispatch = useDispatch();

	const handleSignOut = () => {
		dispatch(signOut())
	}

	return (
		<div onClick={handleSignOut}>
			<Flex alignItems="center" gap={SPACER[2]} >
				<Icon>
					<LogoutOutlined />
				</Icon>
				<span>{props.label}</span>
			</Flex>
		</div>
	)
}

const items = [
	{
		key: 'Edit Profile',
		label: <MenuItem path="/app/pages/setting/edit-profile" label="Edit Profile" icon={<EditOutlined />} />,
	},
	{
		key: 'Help Center',
		label: <MenuItem path="/app/error-page-1" label="Help Center" icon={<QuestionCircleOutlined />} />,
	},
	{
		key: 'Sign Out',
		label: <MenuItemSignOut label="Sign Out" />,
	}
]

export const NavProfile = ({ mode }) => {
	const [user, setUser]=useState('')

	useEffect(()=>{
		async function fetchData() {
			// You can await here
			const data=await AuthService.me()
			setUser(data.data.user[0])
		  }
		  fetchData(user);		
	},[])

	return (
		<Dropdown placement="bottomRight" menu={{ items }} trigger={["click"]}>
			<NavItem mode={mode}>
				<Profile>
					<Avatar src={`https://ui-avatars.com/api/?background=random&&name=${user?.first_name}&&color=fff`} />
					<UserInfo className="profile-text">
						<Name>{user?.first_name}</Name>
						<Title>{user?.role}</Title>
					</UserInfo>
				</Profile>
			</NavItem>
		</Dropdown>
	);
}

export default NavProfile
